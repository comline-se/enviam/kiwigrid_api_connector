//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_import

import 'package:built_collection/built_collection.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:built_value/iso_8601_date_time_serializer.dart';
import 'package:heat_control_api/src/date_serializer.dart';
import 'package:heat_control_api/src/model/date.dart';

import 'package:heat_control_api/src/model/apartment.dart';
import 'package:heat_control_api/src/model/api_state.dart';
import 'package:heat_control_api/src/model/control_unit.dart';
import 'package:heat_control_api/src/model/control_unit_info.dart';
import 'package:heat_control_api/src/model/geolocation.dart';
import 'package:heat_control_api/src/model/heat_level_mapping.dart';
import 'package:heat_control_api/src/model/holiday_program.dart';
import 'package:heat_control_api/src/model/operation_mode.dart';
import 'package:heat_control_api/src/model/room.dart';
import 'package:heat_control_api/src/model/set_room_name_request_body.dart';
import 'package:heat_control_api/src/model/switch_definition.dart';
import 'package:heat_control_api/src/model/time_program.dart';

part 'serializers.g.dart';

@SerializersFor([
  Apartment,
  ApiState,
  ControlUnit,
  ControlUnitInfo,
  Geolocation,
  HeatLevelMapping,
  HolidayProgram,
  OperationMode,
  Room,
  SetRoomNameRequestBody,
  SwitchDefinition,
  TimeProgram,
])
Serializers serializers = (_$serializers.toBuilder()
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(ControlUnit)]),
        () => ListBuilder<ControlUnit>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(Room)]),
        () => ListBuilder<Room>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(HeatLevelMapping)]),
        () => ListBuilder<HeatLevelMapping>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(HolidayProgram)]),
        () => ListBuilder<HolidayProgram>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(TimeProgram)]),
        () => ListBuilder<TimeProgram>(),
      )
      ..add(const DateSerializer())
      ..add(Iso8601DateTimeSerializer()))
    .build();

Serializers standardSerializers =
    (serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
