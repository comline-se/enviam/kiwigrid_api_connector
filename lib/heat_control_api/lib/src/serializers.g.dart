// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'serializers.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(Apartment.serializer)
      ..add(ApiState.serializer)
      ..add(ControlUnit.serializer)
      ..add(ControlUnitInfo.serializer)
      ..add(Geolocation.serializer)
      ..add(HeatLevelMapping.serializer)
      ..add(HolidayProgram.serializer)
      ..add(OperationMode.serializer)
      ..add(Room.serializer)
      ..add(SetRoomNameRequestBody.serializer)
      ..add(SwitchDefinition.serializer)
      ..add(TimeProgram.serializer)
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SwitchDefinition)]),
          () => new ListBuilder<SwitchDefinition>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(TimeProgram)]),
          () => new ListBuilder<TimeProgram>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(Room)]),
          () => new ListBuilder<Room>()))
    .build();

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
