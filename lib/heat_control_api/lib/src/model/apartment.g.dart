// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'apartment.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$Apartment extends Apartment {
  @override
  final String name;
  @override
  final String activeTimeProgram;
  @override
  final double? heatLevel;

  factory _$Apartment([void Function(ApartmentBuilder)? updates]) =>
      (new ApartmentBuilder()..update(updates)).build();

  _$Apartment._(
      {required this.name, required this.activeTimeProgram, this.heatLevel})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(name, 'Apartment', 'name');
    BuiltValueNullFieldError.checkNotNull(
        activeTimeProgram, 'Apartment', 'activeTimeProgram');
  }

  @override
  Apartment rebuild(void Function(ApartmentBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ApartmentBuilder toBuilder() => new ApartmentBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Apartment &&
        name == other.name &&
        activeTimeProgram == other.activeTimeProgram &&
        heatLevel == other.heatLevel;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, name.hashCode), activeTimeProgram.hashCode),
        heatLevel.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Apartment')
          ..add('name', name)
          ..add('activeTimeProgram', activeTimeProgram)
          ..add('heatLevel', heatLevel))
        .toString();
  }
}

class ApartmentBuilder implements Builder<Apartment, ApartmentBuilder> {
  _$Apartment? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _activeTimeProgram;
  String? get activeTimeProgram => _$this._activeTimeProgram;
  set activeTimeProgram(String? activeTimeProgram) =>
      _$this._activeTimeProgram = activeTimeProgram;

  double? _heatLevel;
  double? get heatLevel => _$this._heatLevel;
  set heatLevel(double? heatLevel) => _$this._heatLevel = heatLevel;

  ApartmentBuilder() {
    Apartment._initializeBuilder(this);
  }

  ApartmentBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _activeTimeProgram = $v.activeTimeProgram;
      _heatLevel = $v.heatLevel;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Apartment other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Apartment;
  }

  @override
  void update(void Function(ApartmentBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Apartment build() {
    final _$result = _$v ??
        new _$Apartment._(
            name: BuiltValueNullFieldError.checkNotNull(
                name, 'Apartment', 'name'),
            activeTimeProgram: BuiltValueNullFieldError.checkNotNull(
                activeTimeProgram, 'Apartment', 'activeTimeProgram'),
            heatLevel: heatLevel);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
