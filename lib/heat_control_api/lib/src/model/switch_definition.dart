//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'switch_definition.g.dart';



abstract class SwitchDefinition implements Built<SwitchDefinition, SwitchDefinitionBuilder> {
    @BuiltValueField(wireName: r'id')
    int? get id;

    @BuiltValueField(wireName: r'switchTime')
    String? get switchTime;

    @BuiltValueField(wireName: r'heatLevel')
    double get heatLevel;

    @BuiltValueField(wireName: r'monday')
    bool? get monday;

    @BuiltValueField(wireName: r'tuesday')
    bool? get tuesday;

    @BuiltValueField(wireName: r'wednesday')
    bool? get wednesday;

    @BuiltValueField(wireName: r'thursday')
    bool? get thursday;

    @BuiltValueField(wireName: r'friday')
    bool? get friday;

    @BuiltValueField(wireName: r'saturday')
    bool? get saturday;

    @BuiltValueField(wireName: r'sunday')
    bool? get sunday;

    SwitchDefinition._();

    static void _initializeBuilder(SwitchDefinitionBuilder b) => b;

    factory SwitchDefinition([void updates(SwitchDefinitionBuilder b)]) = _$SwitchDefinition;

    @BuiltValueSerializer(custom: true)
    static Serializer<SwitchDefinition> get serializer => _$SwitchDefinitionSerializer();
}

class _$SwitchDefinitionSerializer implements StructuredSerializer<SwitchDefinition> {
    @override
    final Iterable<Type> types = const [SwitchDefinition, _$SwitchDefinition];

    @override
    final String wireName = r'SwitchDefinition';

    @override
    Iterable<Object?> serialize(Serializers serializers, SwitchDefinition object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(int)));
        }
        if (object.switchTime != null) {
            result
                ..add(r'switchTime')
                ..add(serializers.serialize(object.switchTime,
                    specifiedType: const FullType(String)));
        }
        result
            ..add(r'heatLevel')
            ..add(serializers.serialize(object.heatLevel,
                specifiedType: const FullType(double)));
        if (object.monday != null) {
            result
                ..add(r'monday')
                ..add(serializers.serialize(object.monday,
                    specifiedType: const FullType(bool)));
        }
        if (object.tuesday != null) {
            result
                ..add(r'tuesday')
                ..add(serializers.serialize(object.tuesday,
                    specifiedType: const FullType(bool)));
        }
        if (object.wednesday != null) {
            result
                ..add(r'wednesday')
                ..add(serializers.serialize(object.wednesday,
                    specifiedType: const FullType(bool)));
        }
        if (object.thursday != null) {
            result
                ..add(r'thursday')
                ..add(serializers.serialize(object.thursday,
                    specifiedType: const FullType(bool)));
        }
        if (object.friday != null) {
            result
                ..add(r'friday')
                ..add(serializers.serialize(object.friday,
                    specifiedType: const FullType(bool)));
        }
        if (object.saturday != null) {
            result
                ..add(r'saturday')
                ..add(serializers.serialize(object.saturday,
                    specifiedType: const FullType(bool)));
        }
        if (object.sunday != null) {
            result
                ..add(r'sunday')
                ..add(serializers.serialize(object.sunday,
                    specifiedType: const FullType(bool)));
        }
        return result;
    }

    @override
    SwitchDefinition deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = SwitchDefinitionBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'id':
                    result.id = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    break;
                case r'switchTime':
                    result.switchTime = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'heatLevel':
                    result.heatLevel = serializers.deserialize(value,
                        specifiedType: const FullType(double)) as double;
                    break;
                case r'monday':
                    result.monday = serializers.deserialize(value,
                        specifiedType: const FullType(bool)) as bool;
                    break;
                case r'tuesday':
                    result.tuesday = serializers.deserialize(value,
                        specifiedType: const FullType(bool)) as bool;
                    break;
                case r'wednesday':
                    result.wednesday = serializers.deserialize(value,
                        specifiedType: const FullType(bool)) as bool;
                    break;
                case r'thursday':
                    result.thursday = serializers.deserialize(value,
                        specifiedType: const FullType(bool)) as bool;
                    break;
                case r'friday':
                    result.friday = serializers.deserialize(value,
                        specifiedType: const FullType(bool)) as bool;
                    break;
                case r'saturday':
                    result.saturday = serializers.deserialize(value,
                        specifiedType: const FullType(bool)) as bool;
                    break;
                case r'sunday':
                    result.sunday = serializers.deserialize(value,
                        specifiedType: const FullType(bool)) as bool;
                    break;
            }
        }
        return result.build();
    }
}

