//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'holiday_program.g.dart';



abstract class HolidayProgram implements Built<HolidayProgram, HolidayProgramBuilder> {
    @BuiltValueField(wireName: r'id')
    int? get id;

    @BuiltValueField(wireName: r'name')
    String get name;

    @BuiltValueField(wireName: r'programId')
    int? get programId;

    @BuiltValueField(wireName: r'heatLevel')
    double? get heatLevel;

    @BuiltValueField(wireName: r'startDate')
    DateTime? get startDate;

    @BuiltValueField(wireName: r'endDate')
    DateTime? get endDate;

    @BuiltValueField(wireName: r'active')
    bool? get active;

    HolidayProgram._();

    static void _initializeBuilder(HolidayProgramBuilder b) => b;

    factory HolidayProgram([void updates(HolidayProgramBuilder b)]) = _$HolidayProgram;

    @BuiltValueSerializer(custom: true)
    static Serializer<HolidayProgram> get serializer => _$HolidayProgramSerializer();
}

class _$HolidayProgramSerializer implements StructuredSerializer<HolidayProgram> {
    @override
    final Iterable<Type> types = const [HolidayProgram, _$HolidayProgram];

    @override
    final String wireName = r'HolidayProgram';

    @override
    Iterable<Object?> serialize(Serializers serializers, HolidayProgram object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(int)));
        }
        result
            ..add(r'name')
            ..add(serializers.serialize(object.name,
                specifiedType: const FullType(String)));
        if (object.programId != null) {
            result
                ..add(r'programId')
                ..add(serializers.serialize(object.programId,
                    specifiedType: const FullType(int)));
        }
        if (object.heatLevel != null) {
            result
                ..add(r'heatLevel')
                ..add(serializers.serialize(object.heatLevel,
                    specifiedType: const FullType(double)));
        }
        if (object.startDate != null) {
            result
                ..add(r'startDate')
                ..add(serializers.serialize(object.startDate,
                    specifiedType: const FullType(DateTime)));
        }
        if (object.endDate != null) {
            result
                ..add(r'endDate')
                ..add(serializers.serialize(object.endDate,
                    specifiedType: const FullType(DateTime)));
        }
        if (object.active != null) {
            result
                ..add(r'active')
                ..add(serializers.serialize(object.active,
                    specifiedType: const FullType(bool)));
        }
        return result;
    }

    @override
    HolidayProgram deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = HolidayProgramBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'id':
                    result.id = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    break;
                case r'name':
                    result.name = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'programId':
                    result.programId = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    break;
                case r'heatLevel':
                    result.heatLevel = serializers.deserialize(value,
                        specifiedType: const FullType(double)) as double;
                    break;
                case r'startDate':
                    result.startDate = serializers.deserialize(value,
                        specifiedType: const FullType(DateTime)) as DateTime;
                    break;
                case r'endDate':
                    result.endDate = serializers.deserialize(value,
                        specifiedType: const FullType(DateTime)) as DateTime;
                    break;
                case r'active':
                    result.active = serializers.deserialize(value,
                        specifiedType: const FullType(bool)) as bool;
                    break;
            }
        }
        return result.build();
    }
}

