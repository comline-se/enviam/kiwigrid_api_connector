//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'api_state.g.dart';

class ApiState extends EnumClass {

  @BuiltValueEnumConst(wireName: r'Active')
  static const ApiState active = _$active;
  @BuiltValueEnumConst(wireName: r'Deprecated')
  static const ApiState deprecated = _$deprecated;
  @BuiltValueEnumConst(wireName: r'EndOfLife')
  static const ApiState endOfLife = _$endOfLife;
  @BuiltValueEnumConst(wireName: r'null')
  static const ApiState null_ = _$null_;

  static Serializer<ApiState> get serializer => _$apiStateSerializer;

  const ApiState._(String name): super(name);

  static BuiltSet<ApiState> get values => _$values;
  static ApiState valueOf(String name) => _$valueOf(name);
}

/// Optionally, enum_class can generate a mixin to go with your enum for use
/// with Angular. It exposes your enum constants as getters. So, if you mix it
/// in to your Dart component class, the values become available to the
/// corresponding Angular template.
///
/// Trigger mixin generation by writing a line like this one next to your enum.
abstract class ApiStateMixin = Object with _$ApiStateMixin;

