//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:heat_control_api/src/model/switch_definition.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'time_program.g.dart';



abstract class TimeProgram implements Built<TimeProgram, TimeProgramBuilder> {
    @BuiltValueField(wireName: r'id')
    int? get id;

    @BuiltValueField(wireName: r'name')
    String get name;

    @BuiltValueField(wireName: r'programId')
    int? get programId;

    @BuiltValueField(wireName: r'switchDefinitions')
    BuiltList<SwitchDefinition>? get switchDefinitions;

    TimeProgram._();

    static void _initializeBuilder(TimeProgramBuilder b) => b;

    factory TimeProgram([void updates(TimeProgramBuilder b)]) = _$TimeProgram;

    @BuiltValueSerializer(custom: true)
    static Serializer<TimeProgram> get serializer => _$TimeProgramSerializer();
}

class _$TimeProgramSerializer implements StructuredSerializer<TimeProgram> {
    @override
    final Iterable<Type> types = const [TimeProgram, _$TimeProgram];

    @override
    final String wireName = r'TimeProgram';

    @override
    Iterable<Object?> serialize(Serializers serializers, TimeProgram object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(int)));
        }
        result
            ..add(r'name')
            ..add(serializers.serialize(object.name,
                specifiedType: const FullType(String)));
        if (object.programId != null) {
            result
                ..add(r'programId')
                ..add(serializers.serialize(object.programId,
                    specifiedType: const FullType(int)));
        }
        if (object.switchDefinitions != null) {
            result
                ..add(r'switchDefinitions')
                ..add(serializers.serialize(object.switchDefinitions,
                    specifiedType: const FullType(BuiltList, [FullType(SwitchDefinition)])));
        }
        return result;
    }

    @override
    TimeProgram deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = TimeProgramBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'id':
                    result.id = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    break;
                case r'name':
                    result.name = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'programId':
                    result.programId = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    break;
                case r'switchDefinitions':
                    result.switchDefinitions.replace(serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(SwitchDefinition)])) as BuiltList<SwitchDefinition>);
                    break;
            }
        }
        return result.build();
    }
}

