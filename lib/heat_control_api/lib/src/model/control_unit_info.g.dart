// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'control_unit_info.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ControlUnitInfo extends ControlUnitInfo {
  @override
  final String name;
  @override
  final String? serial;
  @override
  final String? guid;
  @override
  final String gatewayReference;
  @override
  final OperationMode? operationMode;
  @override
  final double? heatLevel;
  @override
  final bool? holidayActive;
  @override
  final int? activeHolidayProgramId;
  @override
  final int? activeTimeProgramId;
  @override
  final String? state;
  @override
  final BuiltList<TimeProgram>? timePrograms;
  @override
  final BuiltList<Room>? rooms;

  factory _$ControlUnitInfo([void Function(ControlUnitInfoBuilder)? updates]) =>
      (new ControlUnitInfoBuilder()..update(updates)).build();

  _$ControlUnitInfo._(
      {required this.name,
      this.serial,
      this.guid,
      required this.gatewayReference,
      this.operationMode,
      this.heatLevel,
      this.holidayActive,
      this.activeHolidayProgramId,
      this.activeTimeProgramId,
      this.state,
      this.timePrograms,
      this.rooms})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(name, 'ControlUnitInfo', 'name');
    BuiltValueNullFieldError.checkNotNull(
        gatewayReference, 'ControlUnitInfo', 'gatewayReference');
  }

  @override
  ControlUnitInfo rebuild(void Function(ControlUnitInfoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ControlUnitInfoBuilder toBuilder() =>
      new ControlUnitInfoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ControlUnitInfo &&
        name == other.name &&
        serial == other.serial &&
        guid == other.guid &&
        gatewayReference == other.gatewayReference &&
        operationMode == other.operationMode &&
        heatLevel == other.heatLevel &&
        holidayActive == other.holidayActive &&
        activeHolidayProgramId == other.activeHolidayProgramId &&
        activeTimeProgramId == other.activeTimeProgramId &&
        state == other.state &&
        timePrograms == other.timePrograms &&
        rooms == other.rooms;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc($jc(0, name.hashCode),
                                                serial.hashCode),
                                            guid.hashCode),
                                        gatewayReference.hashCode),
                                    operationMode.hashCode),
                                heatLevel.hashCode),
                            holidayActive.hashCode),
                        activeHolidayProgramId.hashCode),
                    activeTimeProgramId.hashCode),
                state.hashCode),
            timePrograms.hashCode),
        rooms.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ControlUnitInfo')
          ..add('name', name)
          ..add('serial', serial)
          ..add('guid', guid)
          ..add('gatewayReference', gatewayReference)
          ..add('operationMode', operationMode)
          ..add('heatLevel', heatLevel)
          ..add('holidayActive', holidayActive)
          ..add('activeHolidayProgramId', activeHolidayProgramId)
          ..add('activeTimeProgramId', activeTimeProgramId)
          ..add('state', state)
          ..add('timePrograms', timePrograms)
          ..add('rooms', rooms))
        .toString();
  }
}

class ControlUnitInfoBuilder
    implements Builder<ControlUnitInfo, ControlUnitInfoBuilder> {
  _$ControlUnitInfo? _$v;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _serial;
  String? get serial => _$this._serial;
  set serial(String? serial) => _$this._serial = serial;

  String? _guid;
  String? get guid => _$this._guid;
  set guid(String? guid) => _$this._guid = guid;

  String? _gatewayReference;
  String? get gatewayReference => _$this._gatewayReference;
  set gatewayReference(String? gatewayReference) =>
      _$this._gatewayReference = gatewayReference;

  OperationMode? _operationMode;
  OperationMode? get operationMode => _$this._operationMode;
  set operationMode(OperationMode? operationMode) =>
      _$this._operationMode = operationMode;

  double? _heatLevel;
  double? get heatLevel => _$this._heatLevel;
  set heatLevel(double? heatLevel) => _$this._heatLevel = heatLevel;

  bool? _holidayActive;
  bool? get holidayActive => _$this._holidayActive;
  set holidayActive(bool? holidayActive) =>
      _$this._holidayActive = holidayActive;

  int? _activeHolidayProgramId;
  int? get activeHolidayProgramId => _$this._activeHolidayProgramId;
  set activeHolidayProgramId(int? activeHolidayProgramId) =>
      _$this._activeHolidayProgramId = activeHolidayProgramId;

  int? _activeTimeProgramId;
  int? get activeTimeProgramId => _$this._activeTimeProgramId;
  set activeTimeProgramId(int? activeTimeProgramId) =>
      _$this._activeTimeProgramId = activeTimeProgramId;

  String? _state;
  String? get state => _$this._state;
  set state(String? state) => _$this._state = state;

  ListBuilder<TimeProgram>? _timePrograms;
  ListBuilder<TimeProgram> get timePrograms =>
      _$this._timePrograms ??= new ListBuilder<TimeProgram>();
  set timePrograms(ListBuilder<TimeProgram>? timePrograms) =>
      _$this._timePrograms = timePrograms;

  ListBuilder<Room>? _rooms;
  ListBuilder<Room> get rooms => _$this._rooms ??= new ListBuilder<Room>();
  set rooms(ListBuilder<Room>? rooms) => _$this._rooms = rooms;

  ControlUnitInfoBuilder() {
    ControlUnitInfo._initializeBuilder(this);
  }

  ControlUnitInfoBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _name = $v.name;
      _serial = $v.serial;
      _guid = $v.guid;
      _gatewayReference = $v.gatewayReference;
      _operationMode = $v.operationMode;
      _heatLevel = $v.heatLevel;
      _holidayActive = $v.holidayActive;
      _activeHolidayProgramId = $v.activeHolidayProgramId;
      _activeTimeProgramId = $v.activeTimeProgramId;
      _state = $v.state;
      _timePrograms = $v.timePrograms?.toBuilder();
      _rooms = $v.rooms?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ControlUnitInfo other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ControlUnitInfo;
  }

  @override
  void update(void Function(ControlUnitInfoBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ControlUnitInfo build() {
    _$ControlUnitInfo _$result;
    try {
      _$result = _$v ??
          new _$ControlUnitInfo._(
              name: BuiltValueNullFieldError.checkNotNull(
                  name, 'ControlUnitInfo', 'name'),
              serial: serial,
              guid: guid,
              gatewayReference: BuiltValueNullFieldError.checkNotNull(
                  gatewayReference, 'ControlUnitInfo', 'gatewayReference'),
              operationMode: operationMode,
              heatLevel: heatLevel,
              holidayActive: holidayActive,
              activeHolidayProgramId: activeHolidayProgramId,
              activeTimeProgramId: activeTimeProgramId,
              state: state,
              timePrograms: _timePrograms?.build(),
              rooms: _rooms?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'timePrograms';
        _timePrograms?.build();
        _$failedField = 'rooms';
        _rooms?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ControlUnitInfo', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
