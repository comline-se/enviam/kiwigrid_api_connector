//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:heat_control_api/src/model/time_program.dart';
import 'package:heat_control_api/src/model/operation_mode.dart';
import 'package:built_collection/built_collection.dart';
import 'package:heat_control_api/src/model/room.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'control_unit_info.g.dart';



abstract class ControlUnitInfo implements Built<ControlUnitInfo, ControlUnitInfoBuilder> {
    @BuiltValueField(wireName: r'name')
    String get name;

    @BuiltValueField(wireName: r'serial')
    String? get serial;

    @BuiltValueField(wireName: r'guid')
    String? get guid;

    @BuiltValueField(wireName: r'gatewayReference')
    String get gatewayReference;

    @BuiltValueField(wireName: r'operationMode')
    OperationMode? get operationMode;
    // enum operationModeEnum {  INEXISTENT,  false,  STANDBY,  MANUELL,  MANUELL_LOKAL,  AUTOMATIK,  ZENTRAL,  };

    @BuiltValueField(wireName: r'heatLevel')
    double? get heatLevel;

    /// Indicates if the device has a currently active holiday program
    @BuiltValueField(wireName: r'holidayActive')
    bool? get holidayActive;

    @BuiltValueField(wireName: r'activeHolidayProgramId')
    int? get activeHolidayProgramId;

    @BuiltValueField(wireName: r'activeTimeProgramId')
    int? get activeTimeProgramId;

    @BuiltValueField(wireName: r'state')
    String? get state;

    @BuiltValueField(wireName: r'timePrograms')
    BuiltList<TimeProgram>? get timePrograms;

    @BuiltValueField(wireName: r'rooms')
    BuiltList<Room>? get rooms;

    ControlUnitInfo._();

    static void _initializeBuilder(ControlUnitInfoBuilder b) => b;

    factory ControlUnitInfo([void updates(ControlUnitInfoBuilder b)]) = _$ControlUnitInfo;

    @BuiltValueSerializer(custom: true)
    static Serializer<ControlUnitInfo> get serializer => _$ControlUnitInfoSerializer();
}

class _$ControlUnitInfoSerializer implements StructuredSerializer<ControlUnitInfo> {
    @override
    final Iterable<Type> types = const [ControlUnitInfo, _$ControlUnitInfo];

    @override
    final String wireName = r'ControlUnitInfo';

    @override
    Iterable<Object?> serialize(Serializers serializers, ControlUnitInfo object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'name')
            ..add(serializers.serialize(object.name,
                specifiedType: const FullType(String)));
        if (object.serial != null) {
            result
                ..add(r'serial')
                ..add(serializers.serialize(object.serial,
                    specifiedType: const FullType(String)));
        }
        if (object.guid != null) {
            result
                ..add(r'guid')
                ..add(serializers.serialize(object.guid,
                    specifiedType: const FullType(String)));
        }
        result
            ..add(r'gatewayReference')
            ..add(serializers.serialize(object.gatewayReference,
                specifiedType: const FullType(String)));
        if (object.operationMode != null) {
            result
                ..add(r'operationMode')
                ..add(serializers.serialize(object.operationMode,
                    specifiedType: const FullType(OperationMode)));
        }
        if (object.heatLevel != null) {
            result
                ..add(r'heatLevel')
                ..add(serializers.serialize(object.heatLevel,
                    specifiedType: const FullType(double)));
        }
        if (object.holidayActive != null) {
            result
                ..add(r'holidayActive')
                ..add(serializers.serialize(object.holidayActive,
                    specifiedType: const FullType(bool)));
        }
        if (object.activeHolidayProgramId != null) {
            result
                ..add(r'activeHolidayProgramId')
                ..add(serializers.serialize(object.activeHolidayProgramId,
                    specifiedType: const FullType(int)));
        }
        if (object.activeTimeProgramId != null) {
            result
                ..add(r'activeTimeProgramId')
                ..add(serializers.serialize(object.activeTimeProgramId,
                    specifiedType: const FullType(int)));
        }
        if (object.state != null) {
            result
                ..add(r'state')
                ..add(serializers.serialize(object.state,
                    specifiedType: const FullType(String)));
        }
        if (object.timePrograms != null) {
            result
                ..add(r'timePrograms')
                ..add(serializers.serialize(object.timePrograms,
                    specifiedType: const FullType(BuiltList, [FullType(TimeProgram)])));
        }
        if (object.rooms != null) {
            result
                ..add(r'rooms')
                ..add(serializers.serialize(object.rooms,
                    specifiedType: const FullType(BuiltList, [FullType(Room)])));
        }
        return result;
    }

    @override
    ControlUnitInfo deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = ControlUnitInfoBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'name':
                    result.name = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'serial':
                    result.serial = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'guid':
                    result.guid = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'gatewayReference':
                    result.gatewayReference = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'operationMode':
                    result.operationMode = serializers.deserialize(value,
                        specifiedType: const FullType(OperationMode)) as OperationMode;
                    break;
                case r'heatLevel':
                    result.heatLevel = serializers.deserialize(value,
                        specifiedType: const FullType(double)) as double;
                    break;
                case r'holidayActive':
                    result.holidayActive = serializers.deserialize(value,
                        specifiedType: const FullType(bool)) as bool;
                    break;
                case r'activeHolidayProgramId':
                    result.activeHolidayProgramId = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    break;
                case r'activeTimeProgramId':
                    result.activeTimeProgramId = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    break;
                case r'state':
                    result.state = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'timePrograms':
                    result.timePrograms.replace(serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(TimeProgram)])) as BuiltList<TimeProgram>);
                    break;
                case r'rooms':
                    result.rooms.replace(serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(Room)])) as BuiltList<Room>);
                    break;
            }
        }
        return result.build();
    }
}

