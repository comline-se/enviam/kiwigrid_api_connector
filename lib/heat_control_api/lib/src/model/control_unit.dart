//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:heat_control_api/src/model/geolocation.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'control_unit.g.dart';



abstract class ControlUnit implements Built<ControlUnit, ControlUnitBuilder> {
    @BuiltValueField(wireName: r'id')
    int? get id;

    @BuiltValueField(wireName: r'name')
    String? get name;

    @BuiltValueField(wireName: r'serial')
    String? get serial;

    @BuiltValueField(wireName: r'guid')
    String? get guid;

    @BuiltValueField(wireName: r'state')
    String? get state;

    @BuiltValueField(wireName: r'gatewayReference')
    String? get gatewayReference;

    @BuiltValueField(wireName: r'firmwareVersion')
    String? get firmwareVersion;

    @BuiltValueField(wireName: r'geolocation')
    Geolocation? get geolocation;

    ControlUnit._();

    static void _initializeBuilder(ControlUnitBuilder b) => b;

    factory ControlUnit([void updates(ControlUnitBuilder b)]) = _$ControlUnit;

    @BuiltValueSerializer(custom: true)
    static Serializer<ControlUnit> get serializer => _$ControlUnitSerializer();
}

class _$ControlUnitSerializer implements StructuredSerializer<ControlUnit> {
    @override
    final Iterable<Type> types = const [ControlUnit, _$ControlUnit];

    @override
    final String wireName = r'ControlUnit';

    @override
    Iterable<Object?> serialize(Serializers serializers, ControlUnit object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(int)));
        }
        if (object.name != null) {
            result
                ..add(r'name')
                ..add(serializers.serialize(object.name,
                    specifiedType: const FullType(String)));
        }
        if (object.serial != null) {
            result
                ..add(r'serial')
                ..add(serializers.serialize(object.serial,
                    specifiedType: const FullType(String)));
        }
        if (object.guid != null) {
            result
                ..add(r'guid')
                ..add(serializers.serialize(object.guid,
                    specifiedType: const FullType(String)));
        }
        if (object.state != null) {
            result
                ..add(r'state')
                ..add(serializers.serialize(object.state,
                    specifiedType: const FullType(String)));
        }
        if (object.gatewayReference != null) {
            result
                ..add(r'gatewayReference')
                ..add(serializers.serialize(object.gatewayReference,
                    specifiedType: const FullType(String)));
        }
        if (object.firmwareVersion != null) {
            result
                ..add(r'firmwareVersion')
                ..add(serializers.serialize(object.firmwareVersion,
                    specifiedType: const FullType(String)));
        }
        if (object.geolocation != null) {
            result
                ..add(r'geolocation')
                ..add(serializers.serialize(object.geolocation,
                    specifiedType: const FullType(Geolocation)));
        }
        return result;
    }

    @override
    ControlUnit deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = ControlUnitBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'id':
                    result.id = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    break;
                case r'name':
                    result.name = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'serial':
                    result.serial = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'guid':
                    result.guid = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'state':
                    result.state = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'gatewayReference':
                    result.gatewayReference = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'firmwareVersion':
                    result.firmwareVersion = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'geolocation':
                    result.geolocation.replace(serializers.deserialize(value,
                        specifiedType: const FullType(Geolocation)) as Geolocation);
                    break;
            }
        }
        return result.build();
    }
}

