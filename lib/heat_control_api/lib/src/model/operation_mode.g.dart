// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'operation_mode.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const OperationMode _$INEXISTENT = const OperationMode._('INEXISTENT');
const OperationMode _$false_ = const OperationMode._('false_');
const OperationMode _$STANDBY = const OperationMode._('STANDBY');
const OperationMode _$MANUELL = const OperationMode._('MANUELL');
const OperationMode _$MANUELL_LOKAL = const OperationMode._('MANUELL_LOKAL');
const OperationMode _$AUTOMATIK = const OperationMode._('AUTOMATIK');
const OperationMode _$ZENTRAL = const OperationMode._('ZENTRAL');

OperationMode _$valueOf(String name) {
  switch (name) {
    case 'INEXISTENT':
      return _$INEXISTENT;
    case 'false_':
      return _$false_;
    case 'STANDBY':
      return _$STANDBY;
    case 'MANUELL':
      return _$MANUELL;
    case 'MANUELL_LOKAL':
      return _$MANUELL_LOKAL;
    case 'AUTOMATIK':
      return _$AUTOMATIK;
    case 'ZENTRAL':
      return _$ZENTRAL;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<OperationMode> _$values =
    new BuiltSet<OperationMode>(const <OperationMode>[
  _$INEXISTENT,
  _$false_,
  _$STANDBY,
  _$MANUELL,
  _$MANUELL_LOKAL,
  _$AUTOMATIK,
  _$ZENTRAL,
]);

class _$OperationModeMeta {
  const _$OperationModeMeta();
  OperationMode get INEXISTENT => _$INEXISTENT;
  OperationMode get false_ => _$false_;
  OperationMode get STANDBY => _$STANDBY;
  OperationMode get MANUELL => _$MANUELL;
  OperationMode get MANUELL_LOKAL => _$MANUELL_LOKAL;
  OperationMode get AUTOMATIK => _$AUTOMATIK;
  OperationMode get ZENTRAL => _$ZENTRAL;
  OperationMode valueOf(String name) => _$valueOf(name);
  BuiltSet<OperationMode> get values => _$values;
}

abstract class _$OperationModeMixin {
  // ignore: non_constant_identifier_names
  _$OperationModeMeta get OperationMode => const _$OperationModeMeta();
}

Serializer<OperationMode> _$operationModeSerializer =
    new _$OperationModeSerializer();

class _$OperationModeSerializer implements PrimitiveSerializer<OperationMode> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'INEXISTENT': 'INEXISTENT',
    'false_': 'false',
    'STANDBY': 'STANDBY',
    'MANUELL': 'MANUELL',
    'MANUELL_LOKAL': 'MANUELL_LOKAL',
    'AUTOMATIK': 'AUTOMATIK',
    'ZENTRAL': 'ZENTRAL',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'INEXISTENT': 'INEXISTENT',
    'false': 'false_',
    'STANDBY': 'STANDBY',
    'MANUELL': 'MANUELL',
    'MANUELL_LOKAL': 'MANUELL_LOKAL',
    'AUTOMATIK': 'AUTOMATIK',
    'ZENTRAL': 'ZENTRAL',
  };

  @override
  final Iterable<Type> types = const <Type>[OperationMode];
  @override
  final String wireName = 'OperationMode';

  @override
  Object serialize(Serializers serializers, OperationMode object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  OperationMode deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      OperationMode.valueOf(_fromWire[serialized] ?? serialized as String);
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
