//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'set_room_name_request_body.g.dart';



abstract class SetRoomNameRequestBody implements Built<SetRoomNameRequestBody, SetRoomNameRequestBodyBuilder> {
    @BuiltValueField(wireName: r'name')
    String get name;

    SetRoomNameRequestBody._();

    static void _initializeBuilder(SetRoomNameRequestBodyBuilder b) => b;

    factory SetRoomNameRequestBody([void updates(SetRoomNameRequestBodyBuilder b)]) = _$SetRoomNameRequestBody;

    @BuiltValueSerializer(custom: true)
    static Serializer<SetRoomNameRequestBody> get serializer => _$SetRoomNameRequestBodySerializer();
}

class _$SetRoomNameRequestBodySerializer implements StructuredSerializer<SetRoomNameRequestBody> {
    @override
    final Iterable<Type> types = const [SetRoomNameRequestBody, _$SetRoomNameRequestBody];

    @override
    final String wireName = r'SetRoomNameRequestBody';

    @override
    Iterable<Object?> serialize(Serializers serializers, SetRoomNameRequestBody object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'name')
            ..add(serializers.serialize(object.name,
                specifiedType: const FullType(String)));
        return result;
    }

    @override
    SetRoomNameRequestBody deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = SetRoomNameRequestBodyBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'name':
                    result.name = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
            }
        }
        return result.build();
    }
}

