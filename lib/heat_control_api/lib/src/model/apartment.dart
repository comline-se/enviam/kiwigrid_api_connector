//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'apartment.g.dart';



abstract class Apartment implements Built<Apartment, ApartmentBuilder> {
    @BuiltValueField(wireName: r'name')
    String get name;

    @BuiltValueField(wireName: r'activeTimeProgram')
    String get activeTimeProgram;

    @BuiltValueField(wireName: r'heatLevel')
    double? get heatLevel;

    Apartment._();

    static void _initializeBuilder(ApartmentBuilder b) => b;

    factory Apartment([void updates(ApartmentBuilder b)]) = _$Apartment;

    @BuiltValueSerializer(custom: true)
    static Serializer<Apartment> get serializer => _$ApartmentSerializer();
}

class _$ApartmentSerializer implements StructuredSerializer<Apartment> {
    @override
    final Iterable<Type> types = const [Apartment, _$Apartment];

    @override
    final String wireName = r'Apartment';

    @override
    Iterable<Object?> serialize(Serializers serializers, Apartment object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'name')
            ..add(serializers.serialize(object.name,
                specifiedType: const FullType(String)));
        result
            ..add(r'activeTimeProgram')
            ..add(serializers.serialize(object.activeTimeProgram,
                specifiedType: const FullType(String)));
        if (object.heatLevel != null) {
            result
                ..add(r'heatLevel')
                ..add(serializers.serialize(object.heatLevel,
                    specifiedType: const FullType(double)));
        }
        return result;
    }

    @override
    Apartment deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = ApartmentBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'name':
                    result.name = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'activeTimeProgram':
                    result.activeTimeProgram = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'heatLevel':
                    result.heatLevel = serializers.deserialize(value,
                        specifiedType: const FullType(double)) as double;
                    break;
            }
        }
        return result.build();
    }
}

