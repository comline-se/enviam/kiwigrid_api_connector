// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'geolocation.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$Geolocation extends Geolocation {
  @override
  final double latitude;
  @override
  final double longitude;

  factory _$Geolocation([void Function(GeolocationBuilder)? updates]) =>
      (new GeolocationBuilder()..update(updates)).build();

  _$Geolocation._({required this.latitude, required this.longitude})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(latitude, 'Geolocation', 'latitude');
    BuiltValueNullFieldError.checkNotNull(
        longitude, 'Geolocation', 'longitude');
  }

  @override
  Geolocation rebuild(void Function(GeolocationBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GeolocationBuilder toBuilder() => new GeolocationBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Geolocation &&
        latitude == other.latitude &&
        longitude == other.longitude;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, latitude.hashCode), longitude.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Geolocation')
          ..add('latitude', latitude)
          ..add('longitude', longitude))
        .toString();
  }
}

class GeolocationBuilder implements Builder<Geolocation, GeolocationBuilder> {
  _$Geolocation? _$v;

  double? _latitude;
  double? get latitude => _$this._latitude;
  set latitude(double? latitude) => _$this._latitude = latitude;

  double? _longitude;
  double? get longitude => _$this._longitude;
  set longitude(double? longitude) => _$this._longitude = longitude;

  GeolocationBuilder() {
    Geolocation._initializeBuilder(this);
  }

  GeolocationBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _latitude = $v.latitude;
      _longitude = $v.longitude;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Geolocation other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Geolocation;
  }

  @override
  void update(void Function(GeolocationBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Geolocation build() {
    final _$result = _$v ??
        new _$Geolocation._(
            latitude: BuiltValueNullFieldError.checkNotNull(
                latitude, 'Geolocation', 'latitude'),
            longitude: BuiltValueNullFieldError.checkNotNull(
                longitude, 'Geolocation', 'longitude'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
