// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'time_program.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$TimeProgram extends TimeProgram {
  @override
  final int? id;
  @override
  final String name;
  @override
  final int? programId;
  @override
  final BuiltList<SwitchDefinition>? switchDefinitions;

  factory _$TimeProgram([void Function(TimeProgramBuilder)? updates]) =>
      (new TimeProgramBuilder()..update(updates)).build();

  _$TimeProgram._(
      {this.id, required this.name, this.programId, this.switchDefinitions})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(name, 'TimeProgram', 'name');
  }

  @override
  TimeProgram rebuild(void Function(TimeProgramBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TimeProgramBuilder toBuilder() => new TimeProgramBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TimeProgram &&
        id == other.id &&
        name == other.name &&
        programId == other.programId &&
        switchDefinitions == other.switchDefinitions;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, id.hashCode), name.hashCode), programId.hashCode),
        switchDefinitions.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TimeProgram')
          ..add('id', id)
          ..add('name', name)
          ..add('programId', programId)
          ..add('switchDefinitions', switchDefinitions))
        .toString();
  }
}

class TimeProgramBuilder implements Builder<TimeProgram, TimeProgramBuilder> {
  _$TimeProgram? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  int? _programId;
  int? get programId => _$this._programId;
  set programId(int? programId) => _$this._programId = programId;

  ListBuilder<SwitchDefinition>? _switchDefinitions;
  ListBuilder<SwitchDefinition> get switchDefinitions =>
      _$this._switchDefinitions ??= new ListBuilder<SwitchDefinition>();
  set switchDefinitions(ListBuilder<SwitchDefinition>? switchDefinitions) =>
      _$this._switchDefinitions = switchDefinitions;

  TimeProgramBuilder() {
    TimeProgram._initializeBuilder(this);
  }

  TimeProgramBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _programId = $v.programId;
      _switchDefinitions = $v.switchDefinitions?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TimeProgram other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$TimeProgram;
  }

  @override
  void update(void Function(TimeProgramBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$TimeProgram build() {
    _$TimeProgram _$result;
    try {
      _$result = _$v ??
          new _$TimeProgram._(
              id: id,
              name: BuiltValueNullFieldError.checkNotNull(
                  name, 'TimeProgram', 'name'),
              programId: programId,
              switchDefinitions: _switchDefinitions?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'switchDefinitions';
        _switchDefinitions?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'TimeProgram', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
