// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'switch_definition.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SwitchDefinition extends SwitchDefinition {
  @override
  final int? id;
  @override
  final String? switchTime;
  @override
  final double heatLevel;
  @override
  final bool? monday;
  @override
  final bool? tuesday;
  @override
  final bool? wednesday;
  @override
  final bool? thursday;
  @override
  final bool? friday;
  @override
  final bool? saturday;
  @override
  final bool? sunday;

  factory _$SwitchDefinition(
          [void Function(SwitchDefinitionBuilder)? updates]) =>
      (new SwitchDefinitionBuilder()..update(updates)).build();

  _$SwitchDefinition._(
      {this.id,
      this.switchTime,
      required this.heatLevel,
      this.monday,
      this.tuesday,
      this.wednesday,
      this.thursday,
      this.friday,
      this.saturday,
      this.sunday})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        heatLevel, 'SwitchDefinition', 'heatLevel');
  }

  @override
  SwitchDefinition rebuild(void Function(SwitchDefinitionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SwitchDefinitionBuilder toBuilder() =>
      new SwitchDefinitionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SwitchDefinition &&
        id == other.id &&
        switchTime == other.switchTime &&
        heatLevel == other.heatLevel &&
        monday == other.monday &&
        tuesday == other.tuesday &&
        wednesday == other.wednesday &&
        thursday == other.thursday &&
        friday == other.friday &&
        saturday == other.saturday &&
        sunday == other.sunday;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc($jc(0, id.hashCode),
                                        switchTime.hashCode),
                                    heatLevel.hashCode),
                                monday.hashCode),
                            tuesday.hashCode),
                        wednesday.hashCode),
                    thursday.hashCode),
                friday.hashCode),
            saturday.hashCode),
        sunday.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SwitchDefinition')
          ..add('id', id)
          ..add('switchTime', switchTime)
          ..add('heatLevel', heatLevel)
          ..add('monday', monday)
          ..add('tuesday', tuesday)
          ..add('wednesday', wednesday)
          ..add('thursday', thursday)
          ..add('friday', friday)
          ..add('saturday', saturday)
          ..add('sunday', sunday))
        .toString();
  }
}

class SwitchDefinitionBuilder
    implements Builder<SwitchDefinition, SwitchDefinitionBuilder> {
  _$SwitchDefinition? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _switchTime;
  String? get switchTime => _$this._switchTime;
  set switchTime(String? switchTime) => _$this._switchTime = switchTime;

  double? _heatLevel;
  double? get heatLevel => _$this._heatLevel;
  set heatLevel(double? heatLevel) => _$this._heatLevel = heatLevel;

  bool? _monday;
  bool? get monday => _$this._monday;
  set monday(bool? monday) => _$this._monday = monday;

  bool? _tuesday;
  bool? get tuesday => _$this._tuesday;
  set tuesday(bool? tuesday) => _$this._tuesday = tuesday;

  bool? _wednesday;
  bool? get wednesday => _$this._wednesday;
  set wednesday(bool? wednesday) => _$this._wednesday = wednesday;

  bool? _thursday;
  bool? get thursday => _$this._thursday;
  set thursday(bool? thursday) => _$this._thursday = thursday;

  bool? _friday;
  bool? get friday => _$this._friday;
  set friday(bool? friday) => _$this._friday = friday;

  bool? _saturday;
  bool? get saturday => _$this._saturday;
  set saturday(bool? saturday) => _$this._saturday = saturday;

  bool? _sunday;
  bool? get sunday => _$this._sunday;
  set sunday(bool? sunday) => _$this._sunday = sunday;

  SwitchDefinitionBuilder() {
    SwitchDefinition._initializeBuilder(this);
  }

  SwitchDefinitionBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _switchTime = $v.switchTime;
      _heatLevel = $v.heatLevel;
      _monday = $v.monday;
      _tuesday = $v.tuesday;
      _wednesday = $v.wednesday;
      _thursday = $v.thursday;
      _friday = $v.friday;
      _saturday = $v.saturday;
      _sunday = $v.sunday;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SwitchDefinition other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SwitchDefinition;
  }

  @override
  void update(void Function(SwitchDefinitionBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SwitchDefinition build() {
    final _$result = _$v ??
        new _$SwitchDefinition._(
            id: id,
            switchTime: switchTime,
            heatLevel: BuiltValueNullFieldError.checkNotNull(
                heatLevel, 'SwitchDefinition', 'heatLevel'),
            monday: monday,
            tuesday: tuesday,
            wednesday: wednesday,
            thursday: thursday,
            friday: friday,
            saturday: saturday,
            sunday: sunday);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
