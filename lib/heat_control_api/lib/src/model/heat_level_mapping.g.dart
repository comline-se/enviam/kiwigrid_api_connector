// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'heat_level_mapping.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$HeatLevelMapping extends HeatLevelMapping {
  @override
  final double? heatLevel;
  @override
  final double? temperature;

  factory _$HeatLevelMapping(
          [void Function(HeatLevelMappingBuilder)? updates]) =>
      (new HeatLevelMappingBuilder()..update(updates)).build();

  _$HeatLevelMapping._({this.heatLevel, this.temperature}) : super._();

  @override
  HeatLevelMapping rebuild(void Function(HeatLevelMappingBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  HeatLevelMappingBuilder toBuilder() =>
      new HeatLevelMappingBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is HeatLevelMapping &&
        heatLevel == other.heatLevel &&
        temperature == other.temperature;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, heatLevel.hashCode), temperature.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('HeatLevelMapping')
          ..add('heatLevel', heatLevel)
          ..add('temperature', temperature))
        .toString();
  }
}

class HeatLevelMappingBuilder
    implements Builder<HeatLevelMapping, HeatLevelMappingBuilder> {
  _$HeatLevelMapping? _$v;

  double? _heatLevel;
  double? get heatLevel => _$this._heatLevel;
  set heatLevel(double? heatLevel) => _$this._heatLevel = heatLevel;

  double? _temperature;
  double? get temperature => _$this._temperature;
  set temperature(double? temperature) => _$this._temperature = temperature;

  HeatLevelMappingBuilder() {
    HeatLevelMapping._initializeBuilder(this);
  }

  HeatLevelMappingBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _heatLevel = $v.heatLevel;
      _temperature = $v.temperature;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(HeatLevelMapping other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$HeatLevelMapping;
  }

  @override
  void update(void Function(HeatLevelMappingBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$HeatLevelMapping build() {
    final _$result = _$v ??
        new _$HeatLevelMapping._(
            heatLevel: heatLevel, temperature: temperature);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
