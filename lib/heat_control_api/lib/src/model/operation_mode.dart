//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'operation_mode.g.dart';

class OperationMode extends EnumClass {

  @BuiltValueEnumConst(wireName: r'INEXISTENT')
  static const OperationMode INEXISTENT = _$INEXISTENT;
  @BuiltValueEnumConst(wireName: r'false')
  static const OperationMode false_ = _$false_;
  @BuiltValueEnumConst(wireName: r'STANDBY')
  static const OperationMode STANDBY = _$STANDBY;
  @BuiltValueEnumConst(wireName: r'MANUELL')
  static const OperationMode MANUELL = _$MANUELL;
  @BuiltValueEnumConst(wireName: r'MANUELL_LOKAL')
  static const OperationMode MANUELL_LOKAL = _$MANUELL_LOKAL;
  @BuiltValueEnumConst(wireName: r'AUTOMATIK')
  static const OperationMode AUTOMATIK = _$AUTOMATIK;
  @BuiltValueEnumConst(wireName: r'ZENTRAL')
  static const OperationMode ZENTRAL = _$ZENTRAL;

  static Serializer<OperationMode> get serializer => _$operationModeSerializer;

  const OperationMode._(String name): super(name);

  static BuiltSet<OperationMode> get values => _$values;
  static OperationMode valueOf(String name) => _$valueOf(name);
}

/// Optionally, enum_class can generate a mixin to go with your enum for use
/// with Angular. It exposes your enum constants as getters. So, if you mix it
/// in to your Dart component class, the values become available to the
/// corresponding Angular template.
///
/// Trigger mixin generation by writing a line like this one next to your enum.
abstract class OperationModeMixin = Object with _$OperationModeMixin;

