//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'heat_level_mapping.g.dart';



abstract class HeatLevelMapping implements Built<HeatLevelMapping, HeatLevelMappingBuilder> {
    @BuiltValueField(wireName: r'heatLevel')
    double? get heatLevel;

    @BuiltValueField(wireName: r'temperature')
    double? get temperature;

    HeatLevelMapping._();

    static void _initializeBuilder(HeatLevelMappingBuilder b) => b;

    factory HeatLevelMapping([void updates(HeatLevelMappingBuilder b)]) = _$HeatLevelMapping;

    @BuiltValueSerializer(custom: true)
    static Serializer<HeatLevelMapping> get serializer => _$HeatLevelMappingSerializer();
}

class _$HeatLevelMappingSerializer implements StructuredSerializer<HeatLevelMapping> {
    @override
    final Iterable<Type> types = const [HeatLevelMapping, _$HeatLevelMapping];

    @override
    final String wireName = r'HeatLevelMapping';

    @override
    Iterable<Object?> serialize(Serializers serializers, HeatLevelMapping object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.heatLevel != null) {
            result
                ..add(r'heatLevel')
                ..add(serializers.serialize(object.heatLevel,
                    specifiedType: const FullType(double)));
        }
        if (object.temperature != null) {
            result
                ..add(r'temperature')
                ..add(serializers.serialize(object.temperature,
                    specifiedType: const FullType(double)));
        }
        return result;
    }

    @override
    HeatLevelMapping deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = HeatLevelMappingBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'heatLevel':
                    result.heatLevel = serializers.deserialize(value,
                        specifiedType: const FullType(double)) as double;
                    break;
                case r'temperature':
                    result.temperature = serializers.deserialize(value,
                        specifiedType: const FullType(double)) as double;
                    break;
            }
        }
        return result.build();
    }
}

