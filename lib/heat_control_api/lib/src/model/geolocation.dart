//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'geolocation.g.dart';



abstract class Geolocation implements Built<Geolocation, GeolocationBuilder> {
    @BuiltValueField(wireName: r'latitude')
    double get latitude;

    @BuiltValueField(wireName: r'longitude')
    double get longitude;

    Geolocation._();

    static void _initializeBuilder(GeolocationBuilder b) => b;

    factory Geolocation([void updates(GeolocationBuilder b)]) = _$Geolocation;

    @BuiltValueSerializer(custom: true)
    static Serializer<Geolocation> get serializer => _$GeolocationSerializer();
}

class _$GeolocationSerializer implements StructuredSerializer<Geolocation> {
    @override
    final Iterable<Type> types = const [Geolocation, _$Geolocation];

    @override
    final String wireName = r'Geolocation';

    @override
    Iterable<Object?> serialize(Serializers serializers, Geolocation object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'latitude')
            ..add(serializers.serialize(object.latitude,
                specifiedType: const FullType(double)));
        result
            ..add(r'longitude')
            ..add(serializers.serialize(object.longitude,
                specifiedType: const FullType(double)));
        return result;
    }

    @override
    Geolocation deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = GeolocationBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'latitude':
                    result.latitude = serializers.deserialize(value,
                        specifiedType: const FullType(double)) as double;
                    break;
                case r'longitude':
                    result.longitude = serializers.deserialize(value,
                        specifiedType: const FullType(double)) as double;
                    break;
            }
        }
        return result.build();
    }
}

