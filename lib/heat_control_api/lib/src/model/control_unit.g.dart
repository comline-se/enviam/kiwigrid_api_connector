// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'control_unit.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ControlUnit extends ControlUnit {
  @override
  final int? id;
  @override
  final String? name;
  @override
  final String? serial;
  @override
  final String? guid;
  @override
  final String? state;
  @override
  final String? gatewayReference;
  @override
  final String? firmwareVersion;
  @override
  final Geolocation? geolocation;

  factory _$ControlUnit([void Function(ControlUnitBuilder)? updates]) =>
      (new ControlUnitBuilder()..update(updates)).build();

  _$ControlUnit._(
      {this.id,
      this.name,
      this.serial,
      this.guid,
      this.state,
      this.gatewayReference,
      this.firmwareVersion,
      this.geolocation})
      : super._();

  @override
  ControlUnit rebuild(void Function(ControlUnitBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ControlUnitBuilder toBuilder() => new ControlUnitBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ControlUnit &&
        id == other.id &&
        name == other.name &&
        serial == other.serial &&
        guid == other.guid &&
        state == other.state &&
        gatewayReference == other.gatewayReference &&
        firmwareVersion == other.firmwareVersion &&
        geolocation == other.geolocation;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc($jc($jc(0, id.hashCode), name.hashCode),
                            serial.hashCode),
                        guid.hashCode),
                    state.hashCode),
                gatewayReference.hashCode),
            firmwareVersion.hashCode),
        geolocation.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ControlUnit')
          ..add('id', id)
          ..add('name', name)
          ..add('serial', serial)
          ..add('guid', guid)
          ..add('state', state)
          ..add('gatewayReference', gatewayReference)
          ..add('firmwareVersion', firmwareVersion)
          ..add('geolocation', geolocation))
        .toString();
  }
}

class ControlUnitBuilder implements Builder<ControlUnit, ControlUnitBuilder> {
  _$ControlUnit? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _serial;
  String? get serial => _$this._serial;
  set serial(String? serial) => _$this._serial = serial;

  String? _guid;
  String? get guid => _$this._guid;
  set guid(String? guid) => _$this._guid = guid;

  String? _state;
  String? get state => _$this._state;
  set state(String? state) => _$this._state = state;

  String? _gatewayReference;
  String? get gatewayReference => _$this._gatewayReference;
  set gatewayReference(String? gatewayReference) =>
      _$this._gatewayReference = gatewayReference;

  String? _firmwareVersion;
  String? get firmwareVersion => _$this._firmwareVersion;
  set firmwareVersion(String? firmwareVersion) =>
      _$this._firmwareVersion = firmwareVersion;

  GeolocationBuilder? _geolocation;
  GeolocationBuilder get geolocation =>
      _$this._geolocation ??= new GeolocationBuilder();
  set geolocation(GeolocationBuilder? geolocation) =>
      _$this._geolocation = geolocation;

  ControlUnitBuilder() {
    ControlUnit._initializeBuilder(this);
  }

  ControlUnitBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _serial = $v.serial;
      _guid = $v.guid;
      _state = $v.state;
      _gatewayReference = $v.gatewayReference;
      _firmwareVersion = $v.firmwareVersion;
      _geolocation = $v.geolocation?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ControlUnit other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ControlUnit;
  }

  @override
  void update(void Function(ControlUnitBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ControlUnit build() {
    _$ControlUnit _$result;
    try {
      _$result = _$v ??
          new _$ControlUnit._(
              id: id,
              name: name,
              serial: serial,
              guid: guid,
              state: state,
              gatewayReference: gatewayReference,
              firmwareVersion: firmwareVersion,
              geolocation: _geolocation?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'geolocation';
        _geolocation?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ControlUnit', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
