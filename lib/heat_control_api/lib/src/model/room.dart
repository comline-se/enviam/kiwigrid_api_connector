//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:heat_control_api/src/model/operation_mode.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'room.g.dart';



abstract class Room implements Built<Room, RoomBuilder> {
    @BuiltValueField(wireName: r'id')
    int? get id;

    @BuiltValueField(wireName: r'name')
    String get name;

    @BuiltValueField(wireName: r'roomId')
    int get roomId;

    @BuiltValueField(wireName: r'operationMode')
    OperationMode? get operationMode;
    // enum operationModeEnum {  INEXISTENT,  false,  STANDBY,  MANUELL,  MANUELL_LOKAL,  AUTOMATIK,  ZENTRAL,  };

    @BuiltValueField(wireName: r'heatLevel')
    double? get heatLevel;

    /// The effective heat level represents the heat level that is actually in effect for the room. It can be the same as the heat level (if the room is in operation mode MANUELL for example), but in general it can differ from it. The effective heat level dependes on the operation mode of the room, if the current time falls into a holiday period, and also on the configuration of room 0 (if the room is in mode ZENTRAL).
    @BuiltValueField(wireName: r'effectiveHeatLevel')
    double? get effectiveHeatLevel;

    @BuiltValueField(wireName: r'activeTimeProgramId')
    int? get activeTimeProgramId;

    Room._();

    static void _initializeBuilder(RoomBuilder b) => b;

    factory Room([void updates(RoomBuilder b)]) = _$Room;

    @BuiltValueSerializer(custom: true)
    static Serializer<Room> get serializer => _$RoomSerializer();
}

class _$RoomSerializer implements StructuredSerializer<Room> {
    @override
    final Iterable<Type> types = const [Room, _$Room];

    @override
    final String wireName = r'Room';

    @override
    Iterable<Object?> serialize(Serializers serializers, Room object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(int)));
        }
        result
            ..add(r'name')
            ..add(serializers.serialize(object.name,
                specifiedType: const FullType(String)));
        result
            ..add(r'roomId')
            ..add(serializers.serialize(object.roomId,
                specifiedType: const FullType(int)));
        if (object.operationMode != null) {
            result
                ..add(r'operationMode')
                ..add(serializers.serialize(object.operationMode,
                    specifiedType: const FullType(OperationMode)));
        }
        if (object.heatLevel != null) {
            result
                ..add(r'heatLevel')
                ..add(serializers.serialize(object.heatLevel,
                    specifiedType: const FullType(double)));
        }
        if (object.effectiveHeatLevel != null) {
            result
                ..add(r'effectiveHeatLevel')
                ..add(serializers.serialize(object.effectiveHeatLevel,
                    specifiedType: const FullType(double)));
        }
        if (object.activeTimeProgramId != null) {
            result
                ..add(r'activeTimeProgramId')
                ..add(serializers.serialize(object.activeTimeProgramId,
                    specifiedType: const FullType(int)));
        }
        return result;
    }

    @override
    Room deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = RoomBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'id':
                    result.id = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    break;
                case r'name':
                    result.name = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'roomId':
                    result.roomId = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    break;
                case r'operationMode':
                    result.operationMode = serializers.deserialize(value,
                        specifiedType: const FullType(OperationMode)) as OperationMode;
                    break;
                case r'heatLevel':
                    result.heatLevel = serializers.deserialize(value,
                        specifiedType: const FullType(double)) as double;
                    break;
                case r'effectiveHeatLevel':
                    result.effectiveHeatLevel = serializers.deserialize(value,
                        specifiedType: const FullType(double)) as double;
                    break;
                case r'activeTimeProgramId':
                    result.activeTimeProgramId = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    break;
            }
        }
        return result.build();
    }
}

