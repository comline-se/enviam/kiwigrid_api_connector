// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$Room extends Room {
  @override
  final int? id;
  @override
  final String name;
  @override
  final int roomId;
  @override
  final OperationMode? operationMode;
  @override
  final double? heatLevel;
  @override
  final double? effectiveHeatLevel;
  @override
  final int? activeTimeProgramId;

  factory _$Room([void Function(RoomBuilder)? updates]) =>
      (new RoomBuilder()..update(updates)).build();

  _$Room._(
      {this.id,
      required this.name,
      required this.roomId,
      this.operationMode,
      this.heatLevel,
      this.effectiveHeatLevel,
      this.activeTimeProgramId})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(name, 'Room', 'name');
    BuiltValueNullFieldError.checkNotNull(roomId, 'Room', 'roomId');
  }

  @override
  Room rebuild(void Function(RoomBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RoomBuilder toBuilder() => new RoomBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Room &&
        id == other.id &&
        name == other.name &&
        roomId == other.roomId &&
        operationMode == other.operationMode &&
        heatLevel == other.heatLevel &&
        effectiveHeatLevel == other.effectiveHeatLevel &&
        activeTimeProgramId == other.activeTimeProgramId;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, id.hashCode), name.hashCode),
                        roomId.hashCode),
                    operationMode.hashCode),
                heatLevel.hashCode),
            effectiveHeatLevel.hashCode),
        activeTimeProgramId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Room')
          ..add('id', id)
          ..add('name', name)
          ..add('roomId', roomId)
          ..add('operationMode', operationMode)
          ..add('heatLevel', heatLevel)
          ..add('effectiveHeatLevel', effectiveHeatLevel)
          ..add('activeTimeProgramId', activeTimeProgramId))
        .toString();
  }
}

class RoomBuilder implements Builder<Room, RoomBuilder> {
  _$Room? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  int? _roomId;
  int? get roomId => _$this._roomId;
  set roomId(int? roomId) => _$this._roomId = roomId;

  OperationMode? _operationMode;
  OperationMode? get operationMode => _$this._operationMode;
  set operationMode(OperationMode? operationMode) =>
      _$this._operationMode = operationMode;

  double? _heatLevel;
  double? get heatLevel => _$this._heatLevel;
  set heatLevel(double? heatLevel) => _$this._heatLevel = heatLevel;

  double? _effectiveHeatLevel;
  double? get effectiveHeatLevel => _$this._effectiveHeatLevel;
  set effectiveHeatLevel(double? effectiveHeatLevel) =>
      _$this._effectiveHeatLevel = effectiveHeatLevel;

  int? _activeTimeProgramId;
  int? get activeTimeProgramId => _$this._activeTimeProgramId;
  set activeTimeProgramId(int? activeTimeProgramId) =>
      _$this._activeTimeProgramId = activeTimeProgramId;

  RoomBuilder() {
    Room._initializeBuilder(this);
  }

  RoomBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _roomId = $v.roomId;
      _operationMode = $v.operationMode;
      _heatLevel = $v.heatLevel;
      _effectiveHeatLevel = $v.effectiveHeatLevel;
      _activeTimeProgramId = $v.activeTimeProgramId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Room other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Room;
  }

  @override
  void update(void Function(RoomBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Room build() {
    final _$result = _$v ??
        new _$Room._(
            id: id,
            name: BuiltValueNullFieldError.checkNotNull(name, 'Room', 'name'),
            roomId:
                BuiltValueNullFieldError.checkNotNull(roomId, 'Room', 'roomId'),
            operationMode: operationMode,
            heatLevel: heatLevel,
            effectiveHeatLevel: effectiveHeatLevel,
            activeTimeProgramId: activeTimeProgramId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
