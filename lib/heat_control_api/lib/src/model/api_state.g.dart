// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const ApiState _$active = const ApiState._('active');
const ApiState _$deprecated = const ApiState._('deprecated');
const ApiState _$endOfLife = const ApiState._('endOfLife');
const ApiState _$null_ = const ApiState._('null_');

ApiState _$valueOf(String name) {
  switch (name) {
    case 'active':
      return _$active;
    case 'deprecated':
      return _$deprecated;
    case 'endOfLife':
      return _$endOfLife;
    case 'null_':
      return _$null_;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<ApiState> _$values = new BuiltSet<ApiState>(const <ApiState>[
  _$active,
  _$deprecated,
  _$endOfLife,
  _$null_,
]);

class _$ApiStateMeta {
  const _$ApiStateMeta();
  ApiState get active => _$active;
  ApiState get deprecated => _$deprecated;
  ApiState get endOfLife => _$endOfLife;
  ApiState get null_ => _$null_;
  ApiState valueOf(String name) => _$valueOf(name);
  BuiltSet<ApiState> get values => _$values;
}

abstract class _$ApiStateMixin {
  // ignore: non_constant_identifier_names
  _$ApiStateMeta get ApiState => const _$ApiStateMeta();
}

Serializer<ApiState> _$apiStateSerializer = new _$ApiStateSerializer();

class _$ApiStateSerializer implements PrimitiveSerializer<ApiState> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'active': 'Active',
    'deprecated': 'Deprecated',
    'endOfLife': 'EndOfLife',
    'null_': 'null',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'Active': 'active',
    'Deprecated': 'deprecated',
    'EndOfLife': 'endOfLife',
    'null': 'null_',
  };

  @override
  final Iterable<Type> types = const <Type>[ApiState];
  @override
  final String wireName = 'ApiState';

  @override
  Object serialize(Serializers serializers, ApiState object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  ApiState deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ApiState.valueOf(_fromWire[serialized] ?? serialized as String);
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
