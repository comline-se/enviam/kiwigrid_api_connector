// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'holiday_program.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$HolidayProgram extends HolidayProgram {
  @override
  final int? id;
  @override
  final String name;
  @override
  final int? programId;
  @override
  final double? heatLevel;
  @override
  final DateTime? startDate;
  @override
  final DateTime? endDate;
  @override
  final bool? active;

  factory _$HolidayProgram([void Function(HolidayProgramBuilder)? updates]) =>
      (new HolidayProgramBuilder()..update(updates)).build();

  _$HolidayProgram._(
      {this.id,
      required this.name,
      this.programId,
      this.heatLevel,
      this.startDate,
      this.endDate,
      this.active})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(name, 'HolidayProgram', 'name');
  }

  @override
  HolidayProgram rebuild(void Function(HolidayProgramBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  HolidayProgramBuilder toBuilder() =>
      new HolidayProgramBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is HolidayProgram &&
        id == other.id &&
        name == other.name &&
        programId == other.programId &&
        heatLevel == other.heatLevel &&
        startDate == other.startDate &&
        endDate == other.endDate &&
        active == other.active;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, id.hashCode), name.hashCode),
                        programId.hashCode),
                    heatLevel.hashCode),
                startDate.hashCode),
            endDate.hashCode),
        active.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('HolidayProgram')
          ..add('id', id)
          ..add('name', name)
          ..add('programId', programId)
          ..add('heatLevel', heatLevel)
          ..add('startDate', startDate)
          ..add('endDate', endDate)
          ..add('active', active))
        .toString();
  }
}

class HolidayProgramBuilder
    implements Builder<HolidayProgram, HolidayProgramBuilder> {
  _$HolidayProgram? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  int? _programId;
  int? get programId => _$this._programId;
  set programId(int? programId) => _$this._programId = programId;

  double? _heatLevel;
  double? get heatLevel => _$this._heatLevel;
  set heatLevel(double? heatLevel) => _$this._heatLevel = heatLevel;

  DateTime? _startDate;
  DateTime? get startDate => _$this._startDate;
  set startDate(DateTime? startDate) => _$this._startDate = startDate;

  DateTime? _endDate;
  DateTime? get endDate => _$this._endDate;
  set endDate(DateTime? endDate) => _$this._endDate = endDate;

  bool? _active;
  bool? get active => _$this._active;
  set active(bool? active) => _$this._active = active;

  HolidayProgramBuilder() {
    HolidayProgram._initializeBuilder(this);
  }

  HolidayProgramBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _programId = $v.programId;
      _heatLevel = $v.heatLevel;
      _startDate = $v.startDate;
      _endDate = $v.endDate;
      _active = $v.active;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(HolidayProgram other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$HolidayProgram;
  }

  @override
  void update(void Function(HolidayProgramBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$HolidayProgram build() {
    final _$result = _$v ??
        new _$HolidayProgram._(
            id: id,
            name: BuiltValueNullFieldError.checkNotNull(
                name, 'HolidayProgram', 'name'),
            programId: programId,
            heatLevel: heatLevel,
            startDate: startDate,
            endDate: endDate,
            active: active);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
