//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:dio/dio.dart';
import 'package:built_value/serializer.dart';
import 'package:heat_control_api/src/serializers.dart';
import 'package:heat_control_api/src/auth/api_key_auth.dart';
import 'package:heat_control_api/src/auth/basic_auth.dart';
import 'package:heat_control_api/src/auth/oauth.dart';
import 'package:heat_control_api/src/api/common_api.dart';
import 'package:heat_control_api/src/api/heat_control_api.dart';
import 'package:heat_control_api/src/api/heat_control_room_api.dart';
import 'package:heat_control_api/src/api/heat_control_support_api.dart';
import 'package:heat_control_api/src/api/holiday_program_api.dart';
import 'package:heat_control_api/src/api/time_program_api.dart';

class HeatControlApi {
  static const String basePath = r'http://localhost/api/user';

  final Dio dio;
  final Serializers serializers;

  HeatControlApi({
    Dio? dio,
    Serializers? serializers,
    String? basePathOverride,
    List<Interceptor>? interceptors,
  })  : this.serializers = serializers ?? standardSerializers,
        this.dio = dio ??
            Dio(BaseOptions(
              baseUrl: basePathOverride ?? basePath,
              connectTimeout: 5000,
              receiveTimeout: 3000,
            )) {
    if (interceptors == null) {
      this.dio.interceptors.addAll([
        OAuthInterceptor(),
        BasicAuthInterceptor(),
        ApiKeyAuthInterceptor(),
      ]);
    } else {
      this.dio.interceptors.addAll(interceptors);
    }
  }

  void setOAuthToken(String name, String token) {
    if (this.dio.interceptors.any((i) => i is OAuthInterceptor)) {
      (this.dio.interceptors.firstWhere((i) => i is OAuthInterceptor) as OAuthInterceptor).tokens[name] = token;
    }
  }

  void setBasicAuth(String name, String username, String password) {
    if (this.dio.interceptors.any((i) => i is BasicAuthInterceptor)) {
      (this.dio.interceptors.firstWhere((i) => i is BasicAuthInterceptor) as BasicAuthInterceptor).authInfo[name] = BasicAuthInfo(username, password);
    }
  }

  void setApiKey(String name, String apiKey) {
    if (this.dio.interceptors.any((i) => i is ApiKeyAuthInterceptor)) {
      (this.dio.interceptors.firstWhere((element) => element is ApiKeyAuthInterceptor) as ApiKeyAuthInterceptor).apiKeys[name] = apiKey;
    }
  }

  /// Get CommonApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  CommonApi getCommonApi() {
    return CommonApi(dio, serializers);
  }

  /// Get HeatControlApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  HeatControlApi getHeatControlApi() {
    return HeatControlApi(dio: dio, serializers: serializers);
  }

  /// Get HeatControlRoomApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  HeatControlRoomApi getHeatControlRoomApi() {
    return HeatControlRoomApi(dio, serializers);
  }

  /// Get HeatControlSupportApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  HeatControlSupportApi getHeatControlSupportApi() {
    return HeatControlSupportApi(dio, serializers);
  }

  /// Get HolidayProgramApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  HolidayProgramApi getHolidayProgramApi() {
    return HolidayProgramApi(dio, serializers);
  }

  /// Get TimeProgramApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  TimeProgramApi getTimeProgramApi() {
    return TimeProgramApi(dio, serializers);
  }
}
