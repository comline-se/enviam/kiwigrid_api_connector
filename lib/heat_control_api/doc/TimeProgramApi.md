# heat_control_api.api.TimeProgramApi

## Load the API package
```dart
import 'package:heat_control_api/api.dart';
```

All URIs are relative to *http://localhost/api/user*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createTimeProgramForControlUnit**](TimeProgramApi.md#createtimeprogramforcontrolunit) | **post** /heatControlUnits/{guid}/timePrograms | 
[**deleteTimeProgramForControlUnit**](TimeProgramApi.md#deletetimeprogramforcontrolunit) | **delete** /heatControlUnits/{guid}/timePrograms/{id} | 
[**getDefaultTimeProgram**](TimeProgramApi.md#getdefaulttimeprogram) | **get** /heatControlUnits/{guid}/timePrograms/template | 
[**getTimeProgramForControlUnitById**](TimeProgramApi.md#gettimeprogramforcontrolunitbyid) | **get** /heatControlUnits/{guid}/timePrograms/{id} | 
[**getTimeProgramsForControlUnit**](TimeProgramApi.md#gettimeprogramsforcontrolunit) | **get** /heatControlUnits/{guid}/timePrograms | 
[**updateTimeProgramForControlUnit**](TimeProgramApi.md#updatetimeprogramforcontrolunit) | **put** /heatControlUnits/{guid}/timePrograms/{id} | 


# **createTimeProgramForControlUnit**
> createTimeProgramForControlUnit(guid, body)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new TimeProgramApi();
var guid = guid_example; // String | ID of the control Unit
var body = new TimeProgram(); // TimeProgram | body for the time program

try { 
    api_instance.createTimeProgramForControlUnit(guid, body);
} catch (e) {
    print('Exception when calling TimeProgramApi->createTimeProgramForControlUnit: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **body** | [**TimeProgram**](TimeProgram.md)| body for the time program | 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteTimeProgramForControlUnit**
> deleteTimeProgramForControlUnit(guid, id)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new TimeProgramApi();
var guid = guid_example; // String | ID of the control Unit
var id = 789; // int | ID of the time program

try { 
    api_instance.deleteTimeProgramForControlUnit(guid, id);
} catch (e) {
    print('Exception when calling TimeProgramApi->deleteTimeProgramForControlUnit: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **id** | **int**| ID of the time program | 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getDefaultTimeProgram**
> TimeProgram getDefaultTimeProgram(guid)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new TimeProgramApi();
var guid = guid_example; // String | ID of the control Unit

try { 
    var result = api_instance.getDefaultTimeProgram(guid);
    print(result);
} catch (e) {
    print('Exception when calling TimeProgramApi->getDefaultTimeProgram: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 

### Return type

[**TimeProgram**](TimeProgram.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/heatcontrol-v1.0.0+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getTimeProgramForControlUnitById**
> TimeProgram getTimeProgramForControlUnitById(guid, id)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new TimeProgramApi();
var guid = guid_example; // String | ID of the control Unit
var id = 789; // int | ID of the time program

try { 
    var result = api_instance.getTimeProgramForControlUnitById(guid, id);
    print(result);
} catch (e) {
    print('Exception when calling TimeProgramApi->getTimeProgramForControlUnitById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **id** | **int**| ID of the time program | 

### Return type

[**TimeProgram**](TimeProgram.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/heatcontrol-v1.0.0+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getTimeProgramsForControlUnit**
> BuiltList<TimeProgram> getTimeProgramsForControlUnit(guid)



Returns the time programs for the room with the given ID

### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new TimeProgramApi();
var guid = guid_example; // String | ID of the control Unit

try { 
    var result = api_instance.getTimeProgramsForControlUnit(guid);
    print(result);
} catch (e) {
    print('Exception when calling TimeProgramApi->getTimeProgramsForControlUnit: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 

### Return type

[**BuiltList<TimeProgram>**](TimeProgram.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/heatcontrol-v1.0.0+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateTimeProgramForControlUnit**
> updateTimeProgramForControlUnit(guid, id, body)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new TimeProgramApi();
var guid = guid_example; // String | ID of the control Unit
var id = 789; // int | ID of the time program
var body = new TimeProgram(); // TimeProgram | body for the time program

try { 
    api_instance.updateTimeProgramForControlUnit(guid, id, body);
} catch (e) {
    print('Exception when calling TimeProgramApi->updateTimeProgramForControlUnit: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **id** | **int**| ID of the time program | 
 **body** | [**TimeProgram**](TimeProgram.md)| body for the time program | 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

