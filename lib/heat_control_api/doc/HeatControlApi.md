# heat_control_api.api.HeatControlApi

## Load the API package
```dart
import 'package:heat_control_api/api.dart';
```

All URIs are relative to *http://localhost/api/user*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getApartmentForControlUnit**](HeatControlApi.md#getapartmentforcontrolunit) | **get** /heatControlUnits/{guid}/apartment | 
[**getControlUnitById**](HeatControlApi.md#getcontrolunitbyid) | **get** /heatControlUnits/{guid} | get detailed information about the control unit
[**getControlUnitInfo**](HeatControlApi.md#getcontrolunitinfo) | **get** /heatControlUnits/{guid}/info | 
[**getControlUnits**](HeatControlApi.md#getcontrolunits) | **get** /heatControlUnits | get all control units for the given user
[**setOperationModeForApartment**](HeatControlApi.md#setoperationmodeforapartment) | **post** /heatControlUnits/{guid}/apartment/mode/{mode} | 
[**setTimeProgramForApartment**](HeatControlApi.md#settimeprogramforapartment) | **post** /heatControlUnits/{guid}/apartment/timeProgram/{timeProgramId} | 
[**switchHeatControlOff**](HeatControlApi.md#switchheatcontroloff) | **get** /heatControlUnits/{guid}/switchOff | 
[**switchHeatControlOn**](HeatControlApi.md#switchheatcontrolon) | **get** /heatControlUnits/{guid}/switchOn | 


# **getApartmentForControlUnit**
> Apartment getApartmentForControlUnit(guid)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new HeatControlApi();
var guid = guid_example; // String | ID of the control Unit

try { 
    var result = api_instance.getApartmentForControlUnit(guid);
    print(result);
} catch (e) {
    print('Exception when calling HeatControlApi->getApartmentForControlUnit: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 

### Return type

[**Apartment**](Apartment.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/heatcontrol-v1.0.0+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getControlUnitById**
> ControlUnit getControlUnitById(guid)

get detailed information about the control unit

### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new HeatControlApi();
var guid = guid_example; // String | ID of the control Unit

try { 
    var result = api_instance.getControlUnitById(guid);
    print(result);
} catch (e) {
    print('Exception when calling HeatControlApi->getControlUnitById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 

### Return type

[**ControlUnit**](ControlUnit.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/heatcontrol-v1.0.0+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getControlUnitInfo**
> ControlUnitInfo getControlUnitInfo(guid)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new HeatControlApi();
var guid = guid_example; // String | ID of the control Unit

try { 
    var result = api_instance.getControlUnitInfo(guid);
    print(result);
} catch (e) {
    print('Exception when calling HeatControlApi->getControlUnitInfo: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 

### Return type

[**ControlUnitInfo**](ControlUnitInfo.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/heatcontrol-v1.0.0+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getControlUnits**
> BuiltList<ControlUnit> getControlUnits()

get all control units for the given user

### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new HeatControlApi();

try { 
    var result = api_instance.getControlUnits();
    print(result);
} catch (e) {
    print('Exception when calling HeatControlApi->getControlUnits: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**BuiltList<ControlUnit>**](ControlUnit.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/heatcontrol-v1.0.0+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **setOperationModeForApartment**
> setOperationModeForApartment(guid, mode, level)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new HeatControlApi();
var guid = guid_example; // String | ID of the control Unit
var mode = ; // OperationMode | active time program id
var level = 1.2; // double | heat level which is used if manual operation mode is selected

try { 
    api_instance.setOperationModeForApartment(guid, mode, level);
} catch (e) {
    print('Exception when calling HeatControlApi->setOperationModeForApartment: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **mode** | [**OperationMode**](.md)| active time program id | 
 **level** | **double**| heat level which is used if manual operation mode is selected | [optional] 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **setTimeProgramForApartment**
> setTimeProgramForApartment(guid, timeProgramId)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new HeatControlApi();
var guid = guid_example; // String | ID of the control Unit
var timeProgramId = 789; // int | active time program id

try { 
    api_instance.setTimeProgramForApartment(guid, timeProgramId);
} catch (e) {
    print('Exception when calling HeatControlApi->setTimeProgramForApartment: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **timeProgramId** | **int**| active time program id | 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **switchHeatControlOff**
> switchHeatControlOff(guid)



Stores the current operation mode of room 0 and sets the operation mode of room 0 to \"OFF\". i.e. switches the heat control devices for the entire apartment off.

### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new HeatControlApi();
var guid = guid_example; // String | ID of the control Unit

try { 
    api_instance.switchHeatControlOff(guid);
} catch (e) {
    print('Exception when calling HeatControlApi->switchHeatControlOff: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **switchHeatControlOn**
> switchHeatControlOn(guid)



Restores the last operation mode for room 0. i.e. switches the heat control for the apartment back on.

### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new HeatControlApi();
var guid = guid_example; // String | ID of the control Unit

try { 
    api_instance.switchHeatControlOn(guid);
} catch (e) {
    print('Exception when calling HeatControlApi->switchHeatControlOn: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

