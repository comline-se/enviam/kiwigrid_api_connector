# heat_control_api.model.ControlUnit

## Load the model package
```dart
import 'package:heat_control_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **String** |  | [optional] 
**serial** | **String** |  | [optional] 
**guid** | **String** |  | [optional] 
**state** | **String** |  | [optional] 
**gatewayReference** | **String** |  | [optional] 
**firmwareVersion** | **String** |  | [optional] 
**geolocation** | [**Geolocation**](Geolocation.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


