# heat_control_api.api.HeatControlSupportApi

## Load the API package
```dart
import 'package:heat_control_api/api.dart';
```

All URIs are relative to *http://localhost/api/user*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getHeatLevelMapping**](HeatControlSupportApi.md#getheatlevelmapping) | **get** /heatLevelMapping | get heat Level Mapping


# **getHeatLevelMapping**
> BuiltList<HeatLevelMapping> getHeatLevelMapping()

get heat Level Mapping

### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new HeatControlSupportApi();

try { 
    var result = api_instance.getHeatLevelMapping();
    print(result);
} catch (e) {
    print('Exception when calling HeatControlSupportApi->getHeatLevelMapping: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**BuiltList<HeatLevelMapping>**](HeatLevelMapping.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/heatcontrol-v1.0.0+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

