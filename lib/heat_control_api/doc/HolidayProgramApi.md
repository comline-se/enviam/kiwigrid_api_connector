# heat_control_api.api.HolidayProgramApi

## Load the API package
```dart
import 'package:heat_control_api/api.dart';
```

All URIs are relative to *http://localhost/api/user*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createHolidayProgramForControlUnit**](HolidayProgramApi.md#createholidayprogramforcontrolunit) | **post** /heatControlUnits/{guid}/holidayPrograms | 
[**getHolidayProgramForControlUnitById**](HolidayProgramApi.md#getholidayprogramforcontrolunitbyid) | **get** /heatControlUnits/{guid}/holidayPrograms/{id} | 
[**getHolidayProgramsForControlUnit**](HolidayProgramApi.md#getholidayprogramsforcontrolunit) | **get** /heatControlUnits/{guid}/holidayPrograms | 
[**updateHolidayProgramForControlUnit**](HolidayProgramApi.md#updateholidayprogramforcontrolunit) | **put** /heatControlUnits/{guid}/holidayPrograms/{id} | 


# **createHolidayProgramForControlUnit**
> createHolidayProgramForControlUnit(guid, body)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new HolidayProgramApi();
var guid = guid_example; // String | ID of the control Unit
var body = new HolidayProgram(); // HolidayProgram | body for the time program

try { 
    api_instance.createHolidayProgramForControlUnit(guid, body);
} catch (e) {
    print('Exception when calling HolidayProgramApi->createHolidayProgramForControlUnit: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **body** | [**HolidayProgram**](HolidayProgram.md)| body for the time program | 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getHolidayProgramForControlUnitById**
> HolidayProgram getHolidayProgramForControlUnitById(guid, id)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new HolidayProgramApi();
var guid = guid_example; // String | ID of the control Unit
var id = 789; // int | ID of the holiday program

try { 
    var result = api_instance.getHolidayProgramForControlUnitById(guid, id);
    print(result);
} catch (e) {
    print('Exception when calling HolidayProgramApi->getHolidayProgramForControlUnitById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **id** | **int**| ID of the holiday program | 

### Return type

[**HolidayProgram**](HolidayProgram.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/heatcontrol-v1.0.0+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getHolidayProgramsForControlUnit**
> BuiltList<HolidayProgram> getHolidayProgramsForControlUnit(guid)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new HolidayProgramApi();
var guid = guid_example; // String | ID of the control Unit

try { 
    var result = api_instance.getHolidayProgramsForControlUnit(guid);
    print(result);
} catch (e) {
    print('Exception when calling HolidayProgramApi->getHolidayProgramsForControlUnit: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 

### Return type

[**BuiltList<HolidayProgram>**](HolidayProgram.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/heatcontrol-v1.0.0+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateHolidayProgramForControlUnit**
> updateHolidayProgramForControlUnit(guid, id, body)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new HolidayProgramApi();
var guid = guid_example; // String | ID of the control Unit
var id = 789; // int | ID of the holiday program
var body = new HolidayProgram(); // HolidayProgram | body for the time program

try { 
    api_instance.updateHolidayProgramForControlUnit(guid, id, body);
} catch (e) {
    print('Exception when calling HolidayProgramApi->updateHolidayProgramForControlUnit: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **id** | **int**| ID of the holiday program | 
 **body** | [**HolidayProgram**](HolidayProgram.md)| body for the time program | 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

