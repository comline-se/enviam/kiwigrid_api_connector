# heat_control_api.model.SwitchDefinition

## Load the model package
```dart
import 'package:heat_control_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**switchTime** | **String** |  | [optional] 
**heatLevel** | **double** |  | 
**monday** | **bool** |  | [optional] 
**tuesday** | **bool** |  | [optional] 
**wednesday** | **bool** |  | [optional] 
**thursday** | **bool** |  | [optional] 
**friday** | **bool** |  | [optional] 
**saturday** | **bool** |  | [optional] 
**sunday** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


