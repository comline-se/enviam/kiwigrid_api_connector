# heat_control_api.api.CommonApi

## Load the API package
```dart
import 'package:heat_control_api/api.dart';
```

All URIs are relative to *http://localhost/api/user*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getApiState**](CommonApi.md#getapistate) | **get** /apiState | gets the state of the api


# **getApiState**
> ApiState getApiState()

gets the state of the api

 With this method the client call its api state. This is the only operation where the client has to set no specific Accept-header.

### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new CommonApi();

try { 
    var result = api_instance.getApiState();
    print(result);
} catch (e) {
    print('Exception when calling CommonApi->getApiState: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiState**](ApiState.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

