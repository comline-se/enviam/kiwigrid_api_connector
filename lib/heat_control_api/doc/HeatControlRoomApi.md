# heat_control_api.api.HeatControlRoomApi

## Load the API package
```dart
import 'package:heat_control_api/api.dart';
```

All URIs are relative to *http://localhost/api/user*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getRoomsForControlUnit**](HeatControlRoomApi.md#getroomsforcontrolunit) | **get** /heatControlUnits/{guid}/rooms | 
[**setModeForRoom**](HeatControlRoomApi.md#setmodeforroom) | **post** /heatControlUnits/{guid}/rooms/{roomId}/mode/{mode} | 
[**setNameForRoom**](HeatControlRoomApi.md#setnameforroom) | **post** /heatControlUnits/{guid}/rooms/{roomId}/name | 
[**setTimeProgramForRoom**](HeatControlRoomApi.md#settimeprogramforroom) | **post** /heatControlUnits/{guid}/rooms/{roomId}/timeProgram/{timeProgramId} | 


# **getRoomsForControlUnit**
> BuiltList<Room> getRoomsForControlUnit(guid)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new HeatControlRoomApi();
var guid = guid_example; // String | ID of the control Unit

try { 
    var result = api_instance.getRoomsForControlUnit(guid);
    print(result);
} catch (e) {
    print('Exception when calling HeatControlRoomApi->getRoomsForControlUnit: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 

### Return type

[**BuiltList<Room>**](Room.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/heatcontrol-v1.0.0+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **setModeForRoom**
> setModeForRoom(guid, roomId, mode, level)



Sets the operation mode for the room with the given ID

### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new HeatControlRoomApi();
var guid = guid_example; // String | ID of the control Unit
var roomId = 56; // int | id of the room as represented on the device (0-15)
var mode = ; // OperationMode | mode to set
var level = 1.2; // double | heat level which is used if manual operation mode is selected

try { 
    api_instance.setModeForRoom(guid, roomId, mode, level);
} catch (e) {
    print('Exception when calling HeatControlRoomApi->setModeForRoom: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **roomId** | **int**| id of the room as represented on the device (0-15) | 
 **mode** | [**OperationMode**](.md)| mode to set | 
 **level** | **double**| heat level which is used if manual operation mode is selected | [optional] 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **setNameForRoom**
> setNameForRoom(guid, roomId, setRoomNameRequestBody)



Sets the human readable name for the room with the given ID

### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new HeatControlRoomApi();
var guid = guid_example; // String | ID of the control Unit
var roomId = 56; // int | id of the room as represented on the device (0-15)
var setRoomNameRequestBody = new SetRoomNameRequestBody(); // SetRoomNameRequestBody | body for the name

try { 
    api_instance.setNameForRoom(guid, roomId, setRoomNameRequestBody);
} catch (e) {
    print('Exception when calling HeatControlRoomApi->setNameForRoom: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **roomId** | **int**| id of the room as represented on the device (0-15) | 
 **setRoomNameRequestBody** | [**SetRoomNameRequestBody**](SetRoomNameRequestBody.md)| body for the name | 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **setTimeProgramForRoom**
> setTimeProgramForRoom(guid, roomId, timeProgramId)



### Example 
```dart
import 'package:heat_control_api/api.dart';
// TODO Configure API key authorization: BearerAuth
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('BearerAuth').apiKeyPrefix = 'Bearer';

var api_instance = new HeatControlRoomApi();
var guid = guid_example; // String | ID of the control Unit
var roomId = 56; // int | id of the room as represented on the device (0-15)
var timeProgramId = 789; // int | active time program id

try { 
    api_instance.setTimeProgramForRoom(guid, roomId, timeProgramId);
} catch (e) {
    print('Exception when calling HeatControlRoomApi->setTimeProgramForRoom: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **guid** | **String**| ID of the control Unit | 
 **roomId** | **int**| id of the room as represented on the device (0-15) | 
 **timeProgramId** | **int**| active time program id | 

### Return type

void (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

