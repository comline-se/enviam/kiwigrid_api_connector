# heat_control_api.model.HolidayProgram

## Load the model package
```dart
import 'package:heat_control_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **String** |  | 
**programId** | **int** |  | [optional] 
**heatLevel** | **double** |  | [optional] 
**startDate** | [**DateTime**](DateTime.md) |  | [optional] 
**endDate** | [**DateTime**](DateTime.md) |  | [optional] 
**active** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


