import 'package:test/test.dart';
import 'package:heat_control_api/heat_control_api.dart';

// tests for ControlUnitInfo
void main() {
  final instance = ControlUnitInfoBuilder();
  // TODO add properties to the builder and call build()

  group(ControlUnitInfo, () {
    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // String serial
    test('to test the property `serial`', () async {
      // TODO
    });

    // String guid
    test('to test the property `guid`', () async {
      // TODO
    });

    // String gatewayReference
    test('to test the property `gatewayReference`', () async {
      // TODO
    });

    // OperationMode operationMode
    test('to test the property `operationMode`', () async {
      // TODO
    });

    // double heatLevel
    test('to test the property `heatLevel`', () async {
      // TODO
    });

    // Indicates if the device has a currently active holiday program
    // bool holidayActive
    test('to test the property `holidayActive`', () async {
      // TODO
    });

    // int activeHolidayProgramId
    test('to test the property `activeHolidayProgramId`', () async {
      // TODO
    });

    // int activeTimeProgramId
    test('to test the property `activeTimeProgramId`', () async {
      // TODO
    });

    // String state
    test('to test the property `state`', () async {
      // TODO
    });

    // BuiltList<TimeProgram> timePrograms
    test('to test the property `timePrograms`', () async {
      // TODO
    });

    // BuiltList<Room> rooms
    test('to test the property `rooms`', () async {
      // TODO
    });

  });
}
