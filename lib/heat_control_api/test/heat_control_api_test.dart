import 'package:test/test.dart';
import 'package:heat_control_api/heat_control_api.dart';


/// tests for HeatControlApi
void main() {
  final instance = HeatControlApi().getHeatControlApi();

  group(HeatControlApi, () {
    //Future<Apartment> getApartmentForControlUnit(String guid) async
    test('test getApartmentForControlUnit', () async {
      // TODO
    });

    // get detailed information about the control unit
    //
    //Future<ControlUnit> getControlUnitById(String guid) async
    test('test getControlUnitById', () async {
      // TODO
    });

    //Future<ControlUnitInfo> getControlUnitInfo(String guid) async
    test('test getControlUnitInfo', () async {
      // TODO
    });

    // get all control units for the given user
    //
    //Future<BuiltList<ControlUnit>> getControlUnits() async
    test('test getControlUnits', () async {
      // TODO
    });

    //Future setOperationModeForApartment(String guid, OperationMode mode, { double level }) async
    test('test setOperationModeForApartment', () async {
      // TODO
    });

    //Future setTimeProgramForApartment(String guid, int timeProgramId) async
    test('test setTimeProgramForApartment', () async {
      // TODO
    });

    // Stores the current operation mode of room 0 and sets the operation mode of room 0 to \"OFF\". i.e. switches the heat control devices for the entire apartment off.
    //
    //Future switchHeatControlOff(String guid) async
    test('test switchHeatControlOff', () async {
      // TODO
    });

    // Restores the last operation mode for room 0. i.e. switches the heat control for the apartment back on.
    //
    //Future switchHeatControlOn(String guid) async
    test('test switchHeatControlOn', () async {
      // TODO
    });

  });
}
