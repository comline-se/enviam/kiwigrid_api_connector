import 'package:test/test.dart';
import 'package:heat_control_api/heat_control_api.dart';

// tests for SwitchDefinition
void main() {
  final instance = SwitchDefinitionBuilder();
  // TODO add properties to the builder and call build()

  group(SwitchDefinition, () {
    // int id
    test('to test the property `id`', () async {
      // TODO
    });

    // String switchTime
    test('to test the property `switchTime`', () async {
      // TODO
    });

    // double heatLevel
    test('to test the property `heatLevel`', () async {
      // TODO
    });

    // bool monday
    test('to test the property `monday`', () async {
      // TODO
    });

    // bool tuesday
    test('to test the property `tuesday`', () async {
      // TODO
    });

    // bool wednesday
    test('to test the property `wednesday`', () async {
      // TODO
    });

    // bool thursday
    test('to test the property `thursday`', () async {
      // TODO
    });

    // bool friday
    test('to test the property `friday`', () async {
      // TODO
    });

    // bool saturday
    test('to test the property `saturday`', () async {
      // TODO
    });

    // bool sunday
    test('to test the property `sunday`', () async {
      // TODO
    });

  });
}
