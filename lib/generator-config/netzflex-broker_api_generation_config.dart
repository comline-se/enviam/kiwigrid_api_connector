import 'package:openapi_generator_annotations/openapi_generator_annotations.dart';

@Openapi(
    additionalProperties: AdditionalProperties(
        pubName: 'netzflex_broker_api',
        pubAuthor: 'Flex-API@mitnetz-strom.de',
        pubVersion: '0.7.0-12'),
    inputSpecFile: 'lib/generator-config/netzflex-broker-openapi-spec.yaml',
    alwaysRun: true,
    generatorName: Generator.dioNext,
    outputDirectory: 'lib/netzflex_broker_api')
class HeatControlOpenApiConfig extends OpenapiGeneratorConfig {}
