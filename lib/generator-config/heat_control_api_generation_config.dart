import 'package:openapi_generator_annotations/openapi_generator_annotations.dart';

@Openapi(
    additionalProperties: AdditionalProperties(
        pubName: 'heat_control_api',
        pubAuthor: 'emt@kiwigrid.com',
        pubVersion: '0.0.1'),
    inputSpecFile: 'lib/generator-config/heat-control-openapi-spec.yaml',
    alwaysRun: true,
    generatorName: Generator.dioNext,
    outputDirectory: 'lib/heat_control_api')
class HeatControlOpenApiConfig extends OpenapiGeneratorConfig {}
