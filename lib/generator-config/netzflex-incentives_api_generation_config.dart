import 'package:openapi_generator_annotations/openapi_generator_annotations.dart';

@Openapi(
    additionalProperties: AdditionalProperties(
        pubName: 'netzflex_incentives_api',
        pubAuthor: 'emt@kiwigrid.com',
        pubVersion: '1.1.0-5'),
    inputSpecFile: 'lib/generator-config/netzflex-incentives-openapi-spec.yaml',
    alwaysRun: true,
    generatorName: Generator.dioNext,
    outputDirectory: 'lib/netzflex_incentives_api')
class HeatControlOpenApiConfig extends OpenapiGeneratorConfig {}
