// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'schedule_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ScheduleRequest extends ScheduleRequest {
  @override
  final ScheduleInfo? info;
  @override
  final BuiltList<ScheduleItem> items;

  factory _$ScheduleRequest([void Function(ScheduleRequestBuilder)? updates]) =>
      (new ScheduleRequestBuilder()..update(updates)).build();

  _$ScheduleRequest._({this.info, required this.items}) : super._() {
    BuiltValueNullFieldError.checkNotNull(items, 'ScheduleRequest', 'items');
  }

  @override
  ScheduleRequest rebuild(void Function(ScheduleRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ScheduleRequestBuilder toBuilder() =>
      new ScheduleRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ScheduleRequest &&
        info == other.info &&
        items == other.items;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, info.hashCode), items.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ScheduleRequest')
          ..add('info', info)
          ..add('items', items))
        .toString();
  }
}

class ScheduleRequestBuilder
    implements Builder<ScheduleRequest, ScheduleRequestBuilder> {
  _$ScheduleRequest? _$v;

  ScheduleInfoBuilder? _info;
  ScheduleInfoBuilder get info => _$this._info ??= new ScheduleInfoBuilder();
  set info(ScheduleInfoBuilder? info) => _$this._info = info;

  ListBuilder<ScheduleItem>? _items;
  ListBuilder<ScheduleItem> get items =>
      _$this._items ??= new ListBuilder<ScheduleItem>();
  set items(ListBuilder<ScheduleItem>? items) => _$this._items = items;

  ScheduleRequestBuilder() {
    ScheduleRequest._initializeBuilder(this);
  }

  ScheduleRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _info = $v.info?.toBuilder();
      _items = $v.items.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ScheduleRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ScheduleRequest;
  }

  @override
  void update(void Function(ScheduleRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ScheduleRequest build() {
    _$ScheduleRequest _$result;
    try {
      _$result = _$v ??
          new _$ScheduleRequest._(info: _info?.build(), items: items.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'info';
        _info?.build();
        _$failedField = 'items';
        items.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ScheduleRequest', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
