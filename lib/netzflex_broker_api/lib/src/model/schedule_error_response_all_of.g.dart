// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'schedule_error_response_all_of.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ScheduleErrorResponseAllOf extends ScheduleErrorResponseAllOf {
  @override
  final ScheduleErrorCode? code;
  @override
  final ScheduleItems? restrictions;

  factory _$ScheduleErrorResponseAllOf(
          [void Function(ScheduleErrorResponseAllOfBuilder)? updates]) =>
      (new ScheduleErrorResponseAllOfBuilder()..update(updates)).build();

  _$ScheduleErrorResponseAllOf._({this.code, this.restrictions}) : super._();

  @override
  ScheduleErrorResponseAllOf rebuild(
          void Function(ScheduleErrorResponseAllOfBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ScheduleErrorResponseAllOfBuilder toBuilder() =>
      new ScheduleErrorResponseAllOfBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ScheduleErrorResponseAllOf &&
        code == other.code &&
        restrictions == other.restrictions;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, code.hashCode), restrictions.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ScheduleErrorResponseAllOf')
          ..add('code', code)
          ..add('restrictions', restrictions))
        .toString();
  }
}

class ScheduleErrorResponseAllOfBuilder
    implements
        Builder<ScheduleErrorResponseAllOf, ScheduleErrorResponseAllOfBuilder> {
  _$ScheduleErrorResponseAllOf? _$v;

  ScheduleErrorCode? _code;
  ScheduleErrorCode? get code => _$this._code;
  set code(ScheduleErrorCode? code) => _$this._code = code;

  ScheduleItemsBuilder? _restrictions;
  ScheduleItemsBuilder get restrictions =>
      _$this._restrictions ??= new ScheduleItemsBuilder();
  set restrictions(ScheduleItemsBuilder? restrictions) =>
      _$this._restrictions = restrictions;

  ScheduleErrorResponseAllOfBuilder() {
    ScheduleErrorResponseAllOf._initializeBuilder(this);
  }

  ScheduleErrorResponseAllOfBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _code = $v.code;
      _restrictions = $v.restrictions?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ScheduleErrorResponseAllOf other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ScheduleErrorResponseAllOf;
  }

  @override
  void update(void Function(ScheduleErrorResponseAllOfBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ScheduleErrorResponseAllOf build() {
    _$ScheduleErrorResponseAllOf _$result;
    try {
      _$result = _$v ??
          new _$ScheduleErrorResponseAllOf._(
              code: code, restrictions: _restrictions?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'restrictions';
        _restrictions?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ScheduleErrorResponseAllOf', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
