// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'schedule.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$Schedule extends Schedule {
  @override
  final String? id;
  @override
  final ScheduleItems? confirmedSchedule;

  factory _$Schedule([void Function(ScheduleBuilder)? updates]) =>
      (new ScheduleBuilder()..update(updates)).build();

  _$Schedule._({this.id, this.confirmedSchedule}) : super._();

  @override
  Schedule rebuild(void Function(ScheduleBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ScheduleBuilder toBuilder() => new ScheduleBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Schedule &&
        id == other.id &&
        confirmedSchedule == other.confirmedSchedule;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, id.hashCode), confirmedSchedule.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Schedule')
          ..add('id', id)
          ..add('confirmedSchedule', confirmedSchedule))
        .toString();
  }
}

class ScheduleBuilder implements Builder<Schedule, ScheduleBuilder> {
  _$Schedule? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  ScheduleItemsBuilder? _confirmedSchedule;
  ScheduleItemsBuilder get confirmedSchedule =>
      _$this._confirmedSchedule ??= new ScheduleItemsBuilder();
  set confirmedSchedule(ScheduleItemsBuilder? confirmedSchedule) =>
      _$this._confirmedSchedule = confirmedSchedule;

  ScheduleBuilder() {
    Schedule._initializeBuilder(this);
  }

  ScheduleBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _confirmedSchedule = $v.confirmedSchedule?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Schedule other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Schedule;
  }

  @override
  void update(void Function(ScheduleBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Schedule build() {
    _$Schedule _$result;
    try {
      _$result = _$v ??
          new _$Schedule._(
              id: id, confirmedSchedule: _confirmedSchedule?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'confirmedSchedule';
        _confirmedSchedule?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Schedule', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
