//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:netzflex_broker_api/src/model/schedule_error_code.dart';
import 'package:netzflex_broker_api/src/model/schedule_items.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'schedule_error_response_all_of.g.dart';



abstract class ScheduleErrorResponseAllOf implements Built<ScheduleErrorResponseAllOf, ScheduleErrorResponseAllOfBuilder> {
    @BuiltValueField(wireName: r'code')
    ScheduleErrorCode? get code;
    // enum codeEnum {  ILLEGAL,  INVALID,  REJECTED,  };

    @BuiltValueField(wireName: r'restrictions')
    ScheduleItems? get restrictions;

    ScheduleErrorResponseAllOf._();

    static void _initializeBuilder(ScheduleErrorResponseAllOfBuilder b) => b;

    factory ScheduleErrorResponseAllOf([void updates(ScheduleErrorResponseAllOfBuilder b)]) = _$ScheduleErrorResponseAllOf;

    @BuiltValueSerializer(custom: true)
    static Serializer<ScheduleErrorResponseAllOf> get serializer => _$ScheduleErrorResponseAllOfSerializer();
}

class _$ScheduleErrorResponseAllOfSerializer implements StructuredSerializer<ScheduleErrorResponseAllOf> {
    @override
    final Iterable<Type> types = const [ScheduleErrorResponseAllOf, _$ScheduleErrorResponseAllOf];

    @override
    final String wireName = r'ScheduleErrorResponseAllOf';

    @override
    Iterable<Object?> serialize(Serializers serializers, ScheduleErrorResponseAllOf object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.code != null) {
            result
                ..add(r'code')
                ..add(serializers.serialize(object.code,
                    specifiedType: const FullType(ScheduleErrorCode)));
        }
        if (object.restrictions != null) {
            result
                ..add(r'restrictions')
                ..add(serializers.serialize(object.restrictions,
                    specifiedType: const FullType(ScheduleItems)));
        }
        return result;
    }

    @override
    ScheduleErrorResponseAllOf deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = ScheduleErrorResponseAllOfBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'code':
                    result.code = serializers.deserialize(value,
                        specifiedType: const FullType(ScheduleErrorCode)) as ScheduleErrorCode;
                    break;
                case r'restrictions':
                    result.restrictions.replace(serializers.deserialize(value,
                        specifiedType: const FullType(ScheduleItems)) as ScheduleItems);
                    break;
            }
        }
        return result.build();
    }
}

