// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'schedule_error_code.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const ScheduleErrorCode _$ILLEGAL = const ScheduleErrorCode._('ILLEGAL');
const ScheduleErrorCode _$INVALID = const ScheduleErrorCode._('INVALID');
const ScheduleErrorCode _$REJECTED = const ScheduleErrorCode._('REJECTED');

ScheduleErrorCode _$valueOf(String name) {
  switch (name) {
    case 'ILLEGAL':
      return _$ILLEGAL;
    case 'INVALID':
      return _$INVALID;
    case 'REJECTED':
      return _$REJECTED;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<ScheduleErrorCode> _$values =
    new BuiltSet<ScheduleErrorCode>(const <ScheduleErrorCode>[
  _$ILLEGAL,
  _$INVALID,
  _$REJECTED,
]);

class _$ScheduleErrorCodeMeta {
  const _$ScheduleErrorCodeMeta();
  ScheduleErrorCode get ILLEGAL => _$ILLEGAL;
  ScheduleErrorCode get INVALID => _$INVALID;
  ScheduleErrorCode get REJECTED => _$REJECTED;
  ScheduleErrorCode valueOf(String name) => _$valueOf(name);
  BuiltSet<ScheduleErrorCode> get values => _$values;
}

abstract class _$ScheduleErrorCodeMixin {
  // ignore: non_constant_identifier_names
  _$ScheduleErrorCodeMeta get ScheduleErrorCode =>
      const _$ScheduleErrorCodeMeta();
}

Serializer<ScheduleErrorCode> _$scheduleErrorCodeSerializer =
    new _$ScheduleErrorCodeSerializer();

class _$ScheduleErrorCodeSerializer
    implements PrimitiveSerializer<ScheduleErrorCode> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'ILLEGAL': 'ILLEGAL',
    'INVALID': 'INVALID',
    'REJECTED': 'REJECTED',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'ILLEGAL': 'ILLEGAL',
    'INVALID': 'INVALID',
    'REJECTED': 'REJECTED',
  };

  @override
  final Iterable<Type> types = const <Type>[ScheduleErrorCode];
  @override
  final String wireName = 'ScheduleErrorCode';

  @override
  Object serialize(Serializers serializers, ScheduleErrorCode object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  ScheduleErrorCode deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ScheduleErrorCode.valueOf(_fromWire[serialized] ?? serialized as String);
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
