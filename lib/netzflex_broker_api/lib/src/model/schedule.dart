//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:netzflex_broker_api/src/model/schedule_items.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'schedule.g.dart';



abstract class Schedule implements Built<Schedule, ScheduleBuilder> {
    /// Die eindeutige ID der Lastanfrage.
    @BuiltValueField(wireName: r'id')
    String? get id;

    @BuiltValueField(wireName: r'confirmedSchedule')
    ScheduleItems? get confirmedSchedule;

    Schedule._();

    static void _initializeBuilder(ScheduleBuilder b) => b;

    factory Schedule([void updates(ScheduleBuilder b)]) = _$Schedule;

    @BuiltValueSerializer(custom: true)
    static Serializer<Schedule> get serializer => _$ScheduleSerializer();
}

class _$ScheduleSerializer implements StructuredSerializer<Schedule> {
    @override
    final Iterable<Type> types = const [Schedule, _$Schedule];

    @override
    final String wireName = r'Schedule';

    @override
    Iterable<Object?> serialize(Serializers serializers, Schedule object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(String)));
        }
        if (object.confirmedSchedule != null) {
            result
                ..add(r'confirmedSchedule')
                ..add(serializers.serialize(object.confirmedSchedule,
                    specifiedType: const FullType(ScheduleItems)));
        }
        return result;
    }

    @override
    Schedule deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = ScheduleBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'id':
                    result.id = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'confirmedSchedule':
                    result.confirmedSchedule.replace(serializers.deserialize(value,
                        specifiedType: const FullType(ScheduleItems)) as ScheduleItems);
                    break;
            }
        }
        return result.build();
    }
}

