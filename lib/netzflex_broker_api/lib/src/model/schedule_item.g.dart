// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'schedule_item.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ScheduleItem extends ScheduleItem {
  @override
  final num startTime;
  @override
  final int? duration;
  @override
  final int kW;

  factory _$ScheduleItem([void Function(ScheduleItemBuilder)? updates]) =>
      (new ScheduleItemBuilder()..update(updates)).build();

  _$ScheduleItem._({required this.startTime, this.duration, required this.kW})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        startTime, 'ScheduleItem', 'startTime');
    BuiltValueNullFieldError.checkNotNull(kW, 'ScheduleItem', 'kW');
  }

  @override
  ScheduleItem rebuild(void Function(ScheduleItemBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ScheduleItemBuilder toBuilder() => new ScheduleItemBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ScheduleItem &&
        startTime == other.startTime &&
        duration == other.duration &&
        kW == other.kW;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, startTime.hashCode), duration.hashCode), kW.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ScheduleItem')
          ..add('startTime', startTime)
          ..add('duration', duration)
          ..add('kW', kW))
        .toString();
  }
}

class ScheduleItemBuilder
    implements Builder<ScheduleItem, ScheduleItemBuilder> {
  _$ScheduleItem? _$v;

  num? _startTime;
  num? get startTime => _$this._startTime;
  set startTime(num? startTime) => _$this._startTime = startTime;

  int? _duration;
  int? get duration => _$this._duration;
  set duration(int? duration) => _$this._duration = duration;

  int? _kW;
  int? get kW => _$this._kW;
  set kW(int? kW) => _$this._kW = kW;

  ScheduleItemBuilder() {
    ScheduleItem._initializeBuilder(this);
  }

  ScheduleItemBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _startTime = $v.startTime;
      _duration = $v.duration;
      _kW = $v.kW;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ScheduleItem other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ScheduleItem;
  }

  @override
  void update(void Function(ScheduleItemBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ScheduleItem build() {
    final _$result = _$v ??
        new _$ScheduleItem._(
            startTime: BuiltValueNullFieldError.checkNotNull(
                startTime, 'ScheduleItem', 'startTime'),
            duration: duration,
            kW: BuiltValueNullFieldError.checkNotNull(
                kW, 'ScheduleItem', 'kW'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
