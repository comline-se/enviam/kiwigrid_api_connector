//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:netzflex_broker_api/src/model/schedule_item.dart';
import 'package:netzflex_broker_api/src/model/schedule_info.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'schedule_request.g.dart';



abstract class ScheduleRequest implements Built<ScheduleRequest, ScheduleRequestBuilder> {
    @BuiltValueField(wireName: r'info')
    ScheduleInfo? get info;

    /// Einträge der Lastanfrage.
    @BuiltValueField(wireName: r'items')
    BuiltList<ScheduleItem> get items;

    ScheduleRequest._();

    static void _initializeBuilder(ScheduleRequestBuilder b) => b;

    factory ScheduleRequest([void updates(ScheduleRequestBuilder b)]) = _$ScheduleRequest;

    @BuiltValueSerializer(custom: true)
    static Serializer<ScheduleRequest> get serializer => _$ScheduleRequestSerializer();
}

class _$ScheduleRequestSerializer implements StructuredSerializer<ScheduleRequest> {
    @override
    final Iterable<Type> types = const [ScheduleRequest, _$ScheduleRequest];

    @override
    final String wireName = r'ScheduleRequest';

    @override
    Iterable<Object?> serialize(Serializers serializers, ScheduleRequest object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.info != null) {
            result
                ..add(r'info')
                ..add(serializers.serialize(object.info,
                    specifiedType: const FullType(ScheduleInfo)));
        }
        result
            ..add(r'items')
            ..add(serializers.serialize(object.items,
                specifiedType: const FullType(BuiltList, [FullType(ScheduleItem)])));
        return result;
    }

    @override
    ScheduleRequest deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = ScheduleRequestBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'info':
                    result.info.replace(serializers.deserialize(value,
                        specifiedType: const FullType(ScheduleInfo)) as ScheduleInfo);
                    break;
                case r'items':
                    result.items.replace(serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(ScheduleItem)])) as BuiltList<ScheduleItem>);
                    break;
            }
        }
        return result.build();
    }
}

