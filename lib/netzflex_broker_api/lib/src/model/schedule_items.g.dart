// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'schedule_items.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ScheduleItems extends ScheduleItems {
  @override
  final BuiltList<ScheduleItem> items;

  factory _$ScheduleItems([void Function(ScheduleItemsBuilder)? updates]) =>
      (new ScheduleItemsBuilder()..update(updates)).build();

  _$ScheduleItems._({required this.items}) : super._() {
    BuiltValueNullFieldError.checkNotNull(items, 'ScheduleItems', 'items');
  }

  @override
  ScheduleItems rebuild(void Function(ScheduleItemsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ScheduleItemsBuilder toBuilder() => new ScheduleItemsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ScheduleItems && items == other.items;
  }

  @override
  int get hashCode {
    return $jf($jc(0, items.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ScheduleItems')..add('items', items))
        .toString();
  }
}

class ScheduleItemsBuilder
    implements Builder<ScheduleItems, ScheduleItemsBuilder> {
  _$ScheduleItems? _$v;

  ListBuilder<ScheduleItem>? _items;
  ListBuilder<ScheduleItem> get items =>
      _$this._items ??= new ListBuilder<ScheduleItem>();
  set items(ListBuilder<ScheduleItem>? items) => _$this._items = items;

  ScheduleItemsBuilder() {
    ScheduleItems._initializeBuilder(this);
  }

  ScheduleItemsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _items = $v.items.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ScheduleItems other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ScheduleItems;
  }

  @override
  void update(void Function(ScheduleItemsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ScheduleItems build() {
    _$ScheduleItems _$result;
    try {
      _$result = _$v ?? new _$ScheduleItems._(items: items.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'items';
        items.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ScheduleItems', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
