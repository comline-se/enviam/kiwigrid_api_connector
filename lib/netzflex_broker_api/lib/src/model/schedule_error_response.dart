//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:netzflex_broker_api/src/model/schedule_error_code.dart';
import 'package:netzflex_broker_api/src/model/schedule_error_response_all_of.dart';
import 'package:netzflex_broker_api/src/model/schedule_items.dart';
import 'package:netzflex_broker_api/src/model/error_response.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'schedule_error_response.g.dart';



abstract class ScheduleErrorResponse implements Built<ScheduleErrorResponse, ScheduleErrorResponseBuilder> {
    @BuiltValueField(wireName: r'message')
    String get message;

    @BuiltValueField(wireName: r'code')
    ScheduleErrorCode? get code;
    // enum codeEnum {  ILLEGAL,  INVALID,  REJECTED,  };

    @BuiltValueField(wireName: r'restrictions')
    ScheduleItems? get restrictions;

    ScheduleErrorResponse._();

    static void _initializeBuilder(ScheduleErrorResponseBuilder b) => b;

    factory ScheduleErrorResponse([void updates(ScheduleErrorResponseBuilder b)]) = _$ScheduleErrorResponse;

    @BuiltValueSerializer(custom: true)
    static Serializer<ScheduleErrorResponse> get serializer => _$ScheduleErrorResponseSerializer();
}

class _$ScheduleErrorResponseSerializer implements StructuredSerializer<ScheduleErrorResponse> {
    @override
    final Iterable<Type> types = const [ScheduleErrorResponse, _$ScheduleErrorResponse];

    @override
    final String wireName = r'ScheduleErrorResponse';

    @override
    Iterable<Object?> serialize(Serializers serializers, ScheduleErrorResponse object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'message')
            ..add(serializers.serialize(object.message,
                specifiedType: const FullType(String)));
        if (object.code != null) {
            result
                ..add(r'code')
                ..add(serializers.serialize(object.code,
                    specifiedType: const FullType(ScheduleErrorCode)));
        }
        if (object.restrictions != null) {
            result
                ..add(r'restrictions')
                ..add(serializers.serialize(object.restrictions,
                    specifiedType: const FullType(ScheduleItems)));
        }
        return result;
    }

    @override
    ScheduleErrorResponse deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = ScheduleErrorResponseBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'message':
                    result.message = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'code':
                    result.code = serializers.deserialize(value,
                        specifiedType: const FullType(ScheduleErrorCode)) as ScheduleErrorCode;
                    break;
                case r'restrictions':
                    result.restrictions.replace(serializers.deserialize(value,
                        specifiedType: const FullType(ScheduleItems)) as ScheduleItems);
                    break;
            }
        }
        return result.build();
    }
}

