//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:netzflex_broker_api/src/model/schedule_item.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'schedule_items.g.dart';



abstract class ScheduleItems implements Built<ScheduleItems, ScheduleItemsBuilder> {
    /// Einträge der Lastanfrage.
    @BuiltValueField(wireName: r'items')
    BuiltList<ScheduleItem> get items;

    ScheduleItems._();

    static void _initializeBuilder(ScheduleItemsBuilder b) => b;

    factory ScheduleItems([void updates(ScheduleItemsBuilder b)]) = _$ScheduleItems;

    @BuiltValueSerializer(custom: true)
    static Serializer<ScheduleItems> get serializer => _$ScheduleItemsSerializer();
}

class _$ScheduleItemsSerializer implements StructuredSerializer<ScheduleItems> {
    @override
    final Iterable<Type> types = const [ScheduleItems, _$ScheduleItems];

    @override
    final String wireName = r'ScheduleItems';

    @override
    Iterable<Object?> serialize(Serializers serializers, ScheduleItems object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'items')
            ..add(serializers.serialize(object.items,
                specifiedType: const FullType(BuiltList, [FullType(ScheduleItem)])));
        return result;
    }

    @override
    ScheduleItems deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = ScheduleItemsBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'items':
                    result.items.replace(serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(ScheduleItem)])) as BuiltList<ScheduleItem>);
                    break;
            }
        }
        return result.build();
    }
}

