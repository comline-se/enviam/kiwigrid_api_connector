// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'schedule_error_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ScheduleErrorResponse extends ScheduleErrorResponse {
  @override
  final String message;
  @override
  final ScheduleErrorCode? code;
  @override
  final ScheduleItems? restrictions;

  factory _$ScheduleErrorResponse(
          [void Function(ScheduleErrorResponseBuilder)? updates]) =>
      (new ScheduleErrorResponseBuilder()..update(updates)).build();

  _$ScheduleErrorResponse._(
      {required this.message, this.code, this.restrictions})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        message, 'ScheduleErrorResponse', 'message');
  }

  @override
  ScheduleErrorResponse rebuild(
          void Function(ScheduleErrorResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ScheduleErrorResponseBuilder toBuilder() =>
      new ScheduleErrorResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ScheduleErrorResponse &&
        message == other.message &&
        code == other.code &&
        restrictions == other.restrictions;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, message.hashCode), code.hashCode), restrictions.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ScheduleErrorResponse')
          ..add('message', message)
          ..add('code', code)
          ..add('restrictions', restrictions))
        .toString();
  }
}

class ScheduleErrorResponseBuilder
    implements Builder<ScheduleErrorResponse, ScheduleErrorResponseBuilder> {
  _$ScheduleErrorResponse? _$v;

  String? _message;
  String? get message => _$this._message;
  set message(String? message) => _$this._message = message;

  ScheduleErrorCode? _code;
  ScheduleErrorCode? get code => _$this._code;
  set code(ScheduleErrorCode? code) => _$this._code = code;

  ScheduleItemsBuilder? _restrictions;
  ScheduleItemsBuilder get restrictions =>
      _$this._restrictions ??= new ScheduleItemsBuilder();
  set restrictions(ScheduleItemsBuilder? restrictions) =>
      _$this._restrictions = restrictions;

  ScheduleErrorResponseBuilder() {
    ScheduleErrorResponse._initializeBuilder(this);
  }

  ScheduleErrorResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _message = $v.message;
      _code = $v.code;
      _restrictions = $v.restrictions?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ScheduleErrorResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ScheduleErrorResponse;
  }

  @override
  void update(void Function(ScheduleErrorResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ScheduleErrorResponse build() {
    _$ScheduleErrorResponse _$result;
    try {
      _$result = _$v ??
          new _$ScheduleErrorResponse._(
              message: BuiltValueNullFieldError.checkNotNull(
                  message, 'ScheduleErrorResponse', 'message'),
              code: code,
              restrictions: _restrictions?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'restrictions';
        _restrictions?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ScheduleErrorResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
