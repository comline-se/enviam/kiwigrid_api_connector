//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'schedule_error_code.g.dart';

class ScheduleErrorCode extends EnumClass {

  /// - ILLEGAL: Die Anfrage des Clients wurde grundsätzlich nicht verstanden. Die Eingabe ist in-sich unschlüssig. Beispielsweise ist die Lastanfrage nicht vollständig, widersprüchlich, nicht am Viertelstunden-Zeitraster ausgerichtet, oder anderweitig nicht verarbeitbar. Der Client sollte seine Anfrage nicht wiederholen. - INVALID: Die Anfrage des Clients wurde zwar verstanden, aber kann aus strengen fachlichen Gründen nicht erfüllt werden, da sie eine Änderung außerhalb des zulässigen Rahmens beschreibt. Es wird zum Beispiel versucht eine Änderung der Lastanfrage in der Vergangenheit oder fernen Zukunft herbeizuführen, oder es liegt eine andere fachliche Einschränkung vor. Der Client kann seine Anfrage unter Umständen zu einem anderen Zeitpunkt wiederholen. Die Antwort enthält ggf. einen zuträglichen Hinweis auf eine Alternative. - REJECTED: Die Anfrage des Clients wurde verstanden und war valide, aber wurde aus dienlichen Gründen abgelehnt. Die übermittelte Lastanfrage würde zum Beispiel in Teilen die vorgegebene Kapazität überschreiten, könnte jedoch mittels `force=true` erzwungen werden. Die Antwort enthält eine dienliche Alternative, die ohne `force=true` auskommt.
  @BuiltValueEnumConst(wireName: r'ILLEGAL')
  static const ScheduleErrorCode ILLEGAL = _$ILLEGAL;
  /// - ILLEGAL: Die Anfrage des Clients wurde grundsätzlich nicht verstanden. Die Eingabe ist in-sich unschlüssig. Beispielsweise ist die Lastanfrage nicht vollständig, widersprüchlich, nicht am Viertelstunden-Zeitraster ausgerichtet, oder anderweitig nicht verarbeitbar. Der Client sollte seine Anfrage nicht wiederholen. - INVALID: Die Anfrage des Clients wurde zwar verstanden, aber kann aus strengen fachlichen Gründen nicht erfüllt werden, da sie eine Änderung außerhalb des zulässigen Rahmens beschreibt. Es wird zum Beispiel versucht eine Änderung der Lastanfrage in der Vergangenheit oder fernen Zukunft herbeizuführen, oder es liegt eine andere fachliche Einschränkung vor. Der Client kann seine Anfrage unter Umständen zu einem anderen Zeitpunkt wiederholen. Die Antwort enthält ggf. einen zuträglichen Hinweis auf eine Alternative. - REJECTED: Die Anfrage des Clients wurde verstanden und war valide, aber wurde aus dienlichen Gründen abgelehnt. Die übermittelte Lastanfrage würde zum Beispiel in Teilen die vorgegebene Kapazität überschreiten, könnte jedoch mittels `force=true` erzwungen werden. Die Antwort enthält eine dienliche Alternative, die ohne `force=true` auskommt.
  @BuiltValueEnumConst(wireName: r'INVALID')
  static const ScheduleErrorCode INVALID = _$INVALID;
  /// - ILLEGAL: Die Anfrage des Clients wurde grundsätzlich nicht verstanden. Die Eingabe ist in-sich unschlüssig. Beispielsweise ist die Lastanfrage nicht vollständig, widersprüchlich, nicht am Viertelstunden-Zeitraster ausgerichtet, oder anderweitig nicht verarbeitbar. Der Client sollte seine Anfrage nicht wiederholen. - INVALID: Die Anfrage des Clients wurde zwar verstanden, aber kann aus strengen fachlichen Gründen nicht erfüllt werden, da sie eine Änderung außerhalb des zulässigen Rahmens beschreibt. Es wird zum Beispiel versucht eine Änderung der Lastanfrage in der Vergangenheit oder fernen Zukunft herbeizuführen, oder es liegt eine andere fachliche Einschränkung vor. Der Client kann seine Anfrage unter Umständen zu einem anderen Zeitpunkt wiederholen. Die Antwort enthält ggf. einen zuträglichen Hinweis auf eine Alternative. - REJECTED: Die Anfrage des Clients wurde verstanden und war valide, aber wurde aus dienlichen Gründen abgelehnt. Die übermittelte Lastanfrage würde zum Beispiel in Teilen die vorgegebene Kapazität überschreiten, könnte jedoch mittels `force=true` erzwungen werden. Die Antwort enthält eine dienliche Alternative, die ohne `force=true` auskommt.
  @BuiltValueEnumConst(wireName: r'REJECTED')
  static const ScheduleErrorCode REJECTED = _$REJECTED;

  static Serializer<ScheduleErrorCode> get serializer => _$scheduleErrorCodeSerializer;

  const ScheduleErrorCode._(String name): super(name);

  static BuiltSet<ScheduleErrorCode> get values => _$values;
  static ScheduleErrorCode valueOf(String name) => _$valueOf(name);
}

/// Optionally, enum_class can generate a mixin to go with your enum for use
/// with Angular. It exposes your enum constants as getters. So, if you mix it
/// in to your Dart component class, the values become available to the
/// corresponding Angular template.
///
/// Trigger mixin generation by writing a line like this one next to your enum.
abstract class ScheduleErrorCodeMixin = Object with _$ScheduleErrorCodeMixin;

