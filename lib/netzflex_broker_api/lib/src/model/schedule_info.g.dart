// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'schedule_info.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ScheduleInfo extends ScheduleInfo {
  @override
  final num? startTime;
  @override
  final num? endTime;
  @override
  final int? maxPower;

  factory _$ScheduleInfo([void Function(ScheduleInfoBuilder)? updates]) =>
      (new ScheduleInfoBuilder()..update(updates)).build();

  _$ScheduleInfo._({this.startTime, this.endTime, this.maxPower}) : super._();

  @override
  ScheduleInfo rebuild(void Function(ScheduleInfoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ScheduleInfoBuilder toBuilder() => new ScheduleInfoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ScheduleInfo &&
        startTime == other.startTime &&
        endTime == other.endTime &&
        maxPower == other.maxPower;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, startTime.hashCode), endTime.hashCode), maxPower.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ScheduleInfo')
          ..add('startTime', startTime)
          ..add('endTime', endTime)
          ..add('maxPower', maxPower))
        .toString();
  }
}

class ScheduleInfoBuilder
    implements Builder<ScheduleInfo, ScheduleInfoBuilder> {
  _$ScheduleInfo? _$v;

  num? _startTime;
  num? get startTime => _$this._startTime;
  set startTime(num? startTime) => _$this._startTime = startTime;

  num? _endTime;
  num? get endTime => _$this._endTime;
  set endTime(num? endTime) => _$this._endTime = endTime;

  int? _maxPower;
  int? get maxPower => _$this._maxPower;
  set maxPower(int? maxPower) => _$this._maxPower = maxPower;

  ScheduleInfoBuilder() {
    ScheduleInfo._initializeBuilder(this);
  }

  ScheduleInfoBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _startTime = $v.startTime;
      _endTime = $v.endTime;
      _maxPower = $v.maxPower;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ScheduleInfo other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ScheduleInfo;
  }

  @override
  void update(void Function(ScheduleInfoBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ScheduleInfo build() {
    final _$result = _$v ??
        new _$ScheduleInfo._(
            startTime: startTime, endTime: endTime, maxPower: maxPower);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
