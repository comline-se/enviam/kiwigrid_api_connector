//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'schedule_item.g.dart';



abstract class ScheduleItem implements Built<ScheduleItem, ScheduleItemBuilder> {
    /// Startzeit des Zeitabschnitts.
    @BuiltValueField(wireName: r'startTime')
    num get startTime;

    /// Länge des Zeitabschnitts in Sekunden. Dieser Wert ist optional, muss aber bei Verwendung ein positives Vielfaches von 900s. Anderfalls wird als Standardwert 900s angenommen.
    @BuiltValueField(wireName: r'duration')
    int? get duration;

    /// Leistungswert in kW für den Zeitabschnitt beginnend mit dem Startzeitpunkt.
    @BuiltValueField(wireName: r'kW')
    int get kW;

    ScheduleItem._();

    static void _initializeBuilder(ScheduleItemBuilder b) => b;

    factory ScheduleItem([void updates(ScheduleItemBuilder b)]) = _$ScheduleItem;

    @BuiltValueSerializer(custom: true)
    static Serializer<ScheduleItem> get serializer => _$ScheduleItemSerializer();
}

class _$ScheduleItemSerializer implements StructuredSerializer<ScheduleItem> {
    @override
    final Iterable<Type> types = const [ScheduleItem, _$ScheduleItem];

    @override
    final String wireName = r'ScheduleItem';

    @override
    Iterable<Object?> serialize(Serializers serializers, ScheduleItem object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'startTime')
            ..add(serializers.serialize(object.startTime,
                specifiedType: const FullType(num)));
        if (object.duration != null) {
            result
                ..add(r'duration')
                ..add(serializers.serialize(object.duration,
                    specifiedType: const FullType(int)));
        }
        result
            ..add(r'kW')
            ..add(serializers.serialize(object.kW,
                specifiedType: const FullType(int)));
        return result;
    }

    @override
    ScheduleItem deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = ScheduleItemBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'startTime':
                    result.startTime = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'duration':
                    result.duration = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    break;
                case r'kW':
                    result.kW = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    break;
            }
        }
        return result.build();
    }
}

