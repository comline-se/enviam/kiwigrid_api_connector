//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'schedule_info.g.dart';



abstract class ScheduleInfo implements Built<ScheduleInfo, ScheduleInfoBuilder> {
    /// Frühester Zeitpunkt eines möglichen Stromverbrauchs (Plugin). Der Zeitpunkt sollte in der Zukunft liegen.
    @BuiltValueField(wireName: r'startTime')
    num? get startTime;

    /// Spätester Zeitpunkt eines möglichen Stromverbrauchs (Unplug). Sollte in der Zukunft liegen. Darf nicht vor dem Startzeitpunkt (startTime) liegen.
    @BuiltValueField(wireName: r'endTime')
    num? get endTime;

    /// Maximal mögliche Leistungsaufnahme.
    @BuiltValueField(wireName: r'maxPower')
    int? get maxPower;

    ScheduleInfo._();

    static void _initializeBuilder(ScheduleInfoBuilder b) => b;

    factory ScheduleInfo([void updates(ScheduleInfoBuilder b)]) = _$ScheduleInfo;

    @BuiltValueSerializer(custom: true)
    static Serializer<ScheduleInfo> get serializer => _$ScheduleInfoSerializer();
}

class _$ScheduleInfoSerializer implements StructuredSerializer<ScheduleInfo> {
    @override
    final Iterable<Type> types = const [ScheduleInfo, _$ScheduleInfo];

    @override
    final String wireName = r'ScheduleInfo';

    @override
    Iterable<Object?> serialize(Serializers serializers, ScheduleInfo object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.startTime != null) {
            result
                ..add(r'startTime')
                ..add(serializers.serialize(object.startTime,
                    specifiedType: const FullType(num)));
        }
        if (object.endTime != null) {
            result
                ..add(r'endTime')
                ..add(serializers.serialize(object.endTime,
                    specifiedType: const FullType(num)));
        }
        if (object.maxPower != null) {
            result
                ..add(r'maxPower')
                ..add(serializers.serialize(object.maxPower,
                    specifiedType: const FullType(int)));
        }
        return result;
    }

    @override
    ScheduleInfo deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = ScheduleInfoBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'startTime':
                    result.startTime = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'endTime':
                    result.endTime = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'maxPower':
                    result.maxPower = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    break;
            }
        }
        return result.build();
    }
}

