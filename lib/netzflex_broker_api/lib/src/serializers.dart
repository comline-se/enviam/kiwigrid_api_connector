//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_import

import 'package:built_collection/built_collection.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:built_value/iso_8601_date_time_serializer.dart';
import 'package:netzflex_broker_api/src/date_serializer.dart';
import 'package:netzflex_broker_api/src/model/date.dart';

import 'package:netzflex_broker_api/src/model/error_response.dart';
import 'package:netzflex_broker_api/src/model/schedule.dart';
import 'package:netzflex_broker_api/src/model/schedule_error_code.dart';
import 'package:netzflex_broker_api/src/model/schedule_error_response.dart';
import 'package:netzflex_broker_api/src/model/schedule_error_response_all_of.dart';
import 'package:netzflex_broker_api/src/model/schedule_info.dart';
import 'package:netzflex_broker_api/src/model/schedule_item.dart';
import 'package:netzflex_broker_api/src/model/schedule_items.dart';
import 'package:netzflex_broker_api/src/model/schedule_request.dart';

part 'serializers.g.dart';

@SerializersFor([
  ErrorResponse,
  Schedule,
  ScheduleErrorCode,
  ScheduleErrorResponse,
  ScheduleErrorResponseAllOf,
  ScheduleInfo,
  ScheduleItem,
  ScheduleItems,
  ScheduleRequest,
])
Serializers serializers = (_$serializers.toBuilder()
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(Schedule)]),
        () => ListBuilder<Schedule>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(Schedule)]),
        () => ListBuilder<Schedule>(),
      )
      ..add(const DateSerializer())
      ..add(Iso8601DateTimeSerializer()))
    .build();

Serializers standardSerializers =
    (serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
