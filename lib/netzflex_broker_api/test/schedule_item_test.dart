import 'package:test/test.dart';
import 'package:netzflex_broker_api/netzflex_broker_api.dart';

// tests for ScheduleItem
void main() {
  final instance = ScheduleItemBuilder();
  // TODO add properties to the builder and call build()

  group(ScheduleItem, () {
    // Startzeit des Zeitabschnitts.
    // num startTime
    test('to test the property `startTime`', () async {
      // TODO
    });

    // Länge des Zeitabschnitts in Sekunden. Dieser Wert ist optional, muss aber bei Verwendung ein positives Vielfaches von 900s. Anderfalls wird als Standardwert 900s angenommen.
    // int duration
    test('to test the property `duration`', () async {
      // TODO
    });

    // Leistungswert in kW für den Zeitabschnitt beginnend mit dem Startzeitpunkt.
    // int kW
    test('to test the property `kW`', () async {
      // TODO
    });

  });
}
