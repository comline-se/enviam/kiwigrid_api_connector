import 'package:test/test.dart';
import 'package:netzflex_broker_api/netzflex_broker_api.dart';

// tests for ScheduleErrorResponse
void main() {
  final instance = ScheduleErrorResponseBuilder();
  // TODO add properties to the builder and call build()

  group(ScheduleErrorResponse, () {
    // String message
    test('to test the property `message`', () async {
      // TODO
    });

    // ScheduleErrorCode code
    test('to test the property `code`', () async {
      // TODO
    });

    // ScheduleItems restrictions
    test('to test the property `restrictions`', () async {
      // TODO
    });

  });
}
