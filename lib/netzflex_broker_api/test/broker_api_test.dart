import 'package:test/test.dart';
import 'package:netzflex_broker_api/netzflex_broker_api.dart';


/// tests for BrokerApi
void main() {
  final instance = NetzflexBrokerApi().getBrokerApi();

  group(BrokerApi, () {
    // Annulliert die durch die \"scheduleID\" identifizierte Lastanfrage.  Fachliche Forderungen: - Die zu annullierende Lastanfrage muss vollständig in der Zukunft liegen.  Siehe auch `PUT /schedule/{scheduleId}` zum teilweisen Ändern oder Entfernen zukünftiger 15min Zeitabschnitte.
    //
    //Future cancelSchedule(String scheduleID) async
    test('test cancelSchedule', () async {
      // TODO
    });

    // Gibt die Lastanfrage für die übergebene ID zurück.
    //
    //Future<Schedule> getScheduleByID(String scheduleID) async
    test('test getScheduleByID', () async {
      // TODO
    });

    // Gibt alle Lastanfragen entsprechend den passenden Suchkriterien zurück.
    //
    //Future<BuiltList<Schedule>> getSchedules() async
    test('test getSchedules', () async {
      // TODO
    });

    // Beantragt die Reservierung eines Teils der Netzkapazität gemäß der Lastanfrage.
    //
    //Future<Schedule> planSchedule(ScheduleRequest scheduleRequest, { bool force }) async
    test('test planSchedule', () async {
      // TODO
    });

    // Aktualisiert die durch die \"scheduleID\" identifizierte Lastanfrage.
    //
    //Future<Schedule> updateSchedule(String scheduleID, ScheduleRequest scheduleRequest, { bool force }) async
    test('test updateSchedule', () async {
      // TODO
    });

  });
}
