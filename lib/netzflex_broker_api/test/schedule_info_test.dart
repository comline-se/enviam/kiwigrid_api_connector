import 'package:test/test.dart';
import 'package:netzflex_broker_api/netzflex_broker_api.dart';

// tests for ScheduleInfo
void main() {
  final instance = ScheduleInfoBuilder();
  // TODO add properties to the builder and call build()

  group(ScheduleInfo, () {
    // Frühester Zeitpunkt eines möglichen Stromverbrauchs (Plugin). Der Zeitpunkt sollte in der Zukunft liegen.
    // num startTime
    test('to test the property `startTime`', () async {
      // TODO
    });

    // Spätester Zeitpunkt eines möglichen Stromverbrauchs (Unplug). Sollte in der Zukunft liegen. Darf nicht vor dem Startzeitpunkt (startTime) liegen.
    // num endTime
    test('to test the property `endTime`', () async {
      // TODO
    });

    // Maximal mögliche Leistungsaufnahme.
    // int maxPower
    test('to test the property `maxPower`', () async {
      // TODO
    });

  });
}
