# netzflex_broker_api.model.ScheduleErrorResponse

## Load the model package
```dart
import 'package:netzflex_broker_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **String** |  | 
**code** | [**ScheduleErrorCode**](ScheduleErrorCode.md) |  | [optional] 
**restrictions** | [**ScheduleItems**](ScheduleItems.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


