# netzflex_broker_api.model.ScheduleItem

## Load the model package
```dart
import 'package:netzflex_broker_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startTime** | **num** | Startzeit des Zeitabschnitts. | 
**duration** | **int** | Länge des Zeitabschnitts in Sekunden. Dieser Wert ist optional, muss aber bei Verwendung ein positives Vielfaches von 900s. Anderfalls wird als Standardwert 900s angenommen. | [optional] 
**kW** | **int** | Leistungswert in kW für den Zeitabschnitt beginnend mit dem Startzeitpunkt. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


