# netzflex_broker_api.model.ScheduleInfo

## Load the model package
```dart
import 'package:netzflex_broker_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startTime** | **num** | Frühester Zeitpunkt eines möglichen Stromverbrauchs (Plugin). Der Zeitpunkt sollte in der Zukunft liegen. | [optional] 
**endTime** | **num** | Spätester Zeitpunkt eines möglichen Stromverbrauchs (Unplug). Sollte in der Zukunft liegen. Darf nicht vor dem Startzeitpunkt (startTime) liegen. | [optional] 
**maxPower** | **int** | Maximal mögliche Leistungsaufnahme. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


