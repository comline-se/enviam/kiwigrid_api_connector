# netzflex_broker_api.model.ScheduleRequest

## Load the model package
```dart
import 'package:netzflex_broker_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**info** | [**ScheduleInfo**](ScheduleInfo.md) |  | [optional] 
**items** | [**BuiltList<ScheduleItem>**](ScheduleItem.md) | Einträge der Lastanfrage. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


