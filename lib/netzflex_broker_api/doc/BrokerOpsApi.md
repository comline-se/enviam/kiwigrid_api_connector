# netzflex_broker_api.api.BrokerOpsApi

## Load the API package
```dart
import 'package:netzflex_broker_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getSchedulesByServiceDrop**](BrokerOpsApi.md#getschedulesbyservicedrop) | **get** /ops/schedule | 


# **getSchedulesByServiceDrop**
> BuiltList<Schedule> getSchedulesByServiceDrop(serviceDrop, from, to)



Gibt alle bestätigten Lastanfragen für diesen Niederspannungsabgang im angegebenen Zeitbereich zurück.

### Example 
```dart
import 'package:netzflex_broker_api/api.dart';
// TODO Configure HTTP basic authorization: operator-access
//defaultApiClient.getAuthentication<HttpBasicAuth>('operator-access').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('operator-access').password = 'YOUR_PASSWORD';

var api_instance = new BrokerOpsApi();
var serviceDrop = serviceDrop_example; // String | ID des Niederspannungsabgangs.
var from = 8.14; // num | Beginn des gewählten Zeitraums (inklusive).
var to = 8.14; // num | Ende des gewählten Zeitraums (exklusive).

try { 
    var result = api_instance.getSchedulesByServiceDrop(serviceDrop, from, to);
    print(result);
} catch (e) {
    print('Exception when calling BrokerOpsApi->getSchedulesByServiceDrop: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **serviceDrop** | **String**| ID des Niederspannungsabgangs. | 
 **from** | **num**| Beginn des gewählten Zeitraums (inklusive). | 
 **to** | **num**| Ende des gewählten Zeitraums (exklusive). | 

### Return type

[**BuiltList<Schedule>**](Schedule.md)

### Authorization

[operator-access](../README.md#operator-access)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

