# netzflex_broker_api.model.Schedule

## Load the model package
```dart
import 'package:netzflex_broker_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Die eindeutige ID der Lastanfrage. | [optional] 
**confirmedSchedule** | [**ScheduleItems**](ScheduleItems.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


