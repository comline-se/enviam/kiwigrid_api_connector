# netzflex_broker_api.api.BrokerApi

## Load the API package
```dart
import 'package:netzflex_broker_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cancelSchedule**](BrokerApi.md#cancelschedule) | **delete** /schedule/{scheduleID} | 
[**getScheduleByID**](BrokerApi.md#getschedulebyid) | **get** /schedule/{scheduleID} | 
[**getSchedules**](BrokerApi.md#getschedules) | **get** /schedule | 
[**planSchedule**](BrokerApi.md#planschedule) | **post** /schedule | 
[**updateSchedule**](BrokerApi.md#updateschedule) | **put** /schedule/{scheduleID} | 


# **cancelSchedule**
> cancelSchedule(scheduleID)



Annulliert die durch die \"scheduleID\" identifizierte Lastanfrage.  Fachliche Forderungen: - Die zu annullierende Lastanfrage muss vollständig in der Zukunft liegen.  Siehe auch `PUT /schedule/{scheduleId}` zum teilweisen Ändern oder Entfernen zukünftiger 15min Zeitabschnitte.

### Example 
```dart
import 'package:netzflex_broker_api/api.dart';
// TODO Configure HTTP basic authorization: flexclient-access
//defaultApiClient.getAuthentication<HttpBasicAuth>('flexclient-access').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('flexclient-access').password = 'YOUR_PASSWORD';

var api_instance = new BrokerApi();
var scheduleID = scheduleID_example; // String | ID einer bestätigen Lastanfrage.

try { 
    api_instance.cancelSchedule(scheduleID);
} catch (e) {
    print('Exception when calling BrokerApi->cancelSchedule: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scheduleID** | **String**| ID einer bestätigen Lastanfrage. | 

### Return type

void (empty response body)

### Authorization

[flexclient-access](../README.md#flexclient-access)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getScheduleByID**
> Schedule getScheduleByID(scheduleID)



Gibt die Lastanfrage für die übergebene ID zurück.

### Example 
```dart
import 'package:netzflex_broker_api/api.dart';
// TODO Configure HTTP basic authorization: flexclient-access
//defaultApiClient.getAuthentication<HttpBasicAuth>('flexclient-access').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('flexclient-access').password = 'YOUR_PASSWORD';

var api_instance = new BrokerApi();
var scheduleID = scheduleID_example; // String | ID einer bestätigen Lastanfrage.

try { 
    var result = api_instance.getScheduleByID(scheduleID);
    print(result);
} catch (e) {
    print('Exception when calling BrokerApi->getScheduleByID: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scheduleID** | **String**| ID einer bestätigen Lastanfrage. | 

### Return type

[**Schedule**](Schedule.md)

### Authorization

[flexclient-access](../README.md#flexclient-access)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getSchedules**
> BuiltList<Schedule> getSchedules()



Gibt alle Lastanfragen entsprechend den passenden Suchkriterien zurück.

### Example 
```dart
import 'package:netzflex_broker_api/api.dart';
// TODO Configure HTTP basic authorization: flexclient-access
//defaultApiClient.getAuthentication<HttpBasicAuth>('flexclient-access').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('flexclient-access').password = 'YOUR_PASSWORD';

var api_instance = new BrokerApi();

try { 
    var result = api_instance.getSchedules();
    print(result);
} catch (e) {
    print('Exception when calling BrokerApi->getSchedules: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**BuiltList<Schedule>**](Schedule.md)

### Authorization

[flexclient-access](../README.md#flexclient-access)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **planSchedule**
> Schedule planSchedule(scheduleRequest, force)



Beantragt die Reservierung eines Teils der Netzkapazität gemäß der Lastanfrage.

### Example 
```dart
import 'package:netzflex_broker_api/api.dart';
// TODO Configure HTTP basic authorization: flexclient-access
//defaultApiClient.getAuthentication<HttpBasicAuth>('flexclient-access').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('flexclient-access').password = 'YOUR_PASSWORD';

var api_instance = new BrokerApi();
var scheduleRequest = new ScheduleRequest(); // ScheduleRequest | Die übermittelte Lastanfrage.  Technische Forderungen: - Die Lastanfrage darf keine negativen Leistungswerte enthalten. - Die enthaltenen Zeitabschnitte müssen sich am Viertelstunden-Zeitraster orientieren.  Fachliche Forderungen: - Der zeitigste 15min Zeitabschnitt der Lastanfrage darf nicht vor der aktuellen Viertelstunde liegen. - Der späteste 15min Zeitabschnitt der Lastanfrage darf nicht weiter als zwei Tage in der Zukunft liegen.
var force = true; // bool | Steuert die API dahingehend, ob die übermittelte Lastanfrage ohne eine Kapazitätsprüfung aktzeptiert werden soll oder nicht.  Gültige Werte sind: - false: Es wird neben einer Eingabeprüfung der übermittelten Lastanfrage auch eine Kapazitätsprüfung durchgeführt. Dabei handelt es sich um das Standardverhalten der API. - true: Die übermittelte Lastanfrage wird ohne Kapazitätsprüfung aktzeptiert. Es wird lediglich eine fachliche Eingabeprüfung durchgeführt.  Wenn dieser Parameter nicht gesetzt ist, wird der Default-Wert 'false' angenommen.

try { 
    var result = api_instance.planSchedule(scheduleRequest, force);
    print(result);
} catch (e) {
    print('Exception when calling BrokerApi->planSchedule: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scheduleRequest** | [**ScheduleRequest**](ScheduleRequest.md)| Die übermittelte Lastanfrage.  Technische Forderungen: - Die Lastanfrage darf keine negativen Leistungswerte enthalten. - Die enthaltenen Zeitabschnitte müssen sich am Viertelstunden-Zeitraster orientieren.  Fachliche Forderungen: - Der zeitigste 15min Zeitabschnitt der Lastanfrage darf nicht vor der aktuellen Viertelstunde liegen. - Der späteste 15min Zeitabschnitt der Lastanfrage darf nicht weiter als zwei Tage in der Zukunft liegen. | 
 **force** | **bool**| Steuert die API dahingehend, ob die übermittelte Lastanfrage ohne eine Kapazitätsprüfung aktzeptiert werden soll oder nicht.  Gültige Werte sind: - false: Es wird neben einer Eingabeprüfung der übermittelten Lastanfrage auch eine Kapazitätsprüfung durchgeführt. Dabei handelt es sich um das Standardverhalten der API. - true: Die übermittelte Lastanfrage wird ohne Kapazitätsprüfung aktzeptiert. Es wird lediglich eine fachliche Eingabeprüfung durchgeführt.  Wenn dieser Parameter nicht gesetzt ist, wird der Default-Wert 'false' angenommen. | [optional] [default to false]

### Return type

[**Schedule**](Schedule.md)

### Authorization

[flexclient-access](../README.md#flexclient-access)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateSchedule**
> Schedule updateSchedule(scheduleID, scheduleRequest, force)



Aktualisiert die durch die \"scheduleID\" identifizierte Lastanfrage.

### Example 
```dart
import 'package:netzflex_broker_api/api.dart';
// TODO Configure HTTP basic authorization: flexclient-access
//defaultApiClient.getAuthentication<HttpBasicAuth>('flexclient-access').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('flexclient-access').password = 'YOUR_PASSWORD';

var api_instance = new BrokerApi();
var scheduleID = scheduleID_example; // String | ID einer bestätigen Lastanfrage.
var scheduleRequest = new ScheduleRequest(); // ScheduleRequest | Die zu aktualisierende Lastanfrage.  Technische Forderungen: - Die Lastanfrage darf keine negativen Leistungswerte enthalten. - Die enthaltenen Zeitabschnitte müssen sich am Viertelstunden-Zeitraster orientieren.  Fachliche Forderungen: - Alle bestehende 15min Zeitabschnitte der Lastanfrage, die nunmehr in der Vergangenheit liegen, müssen unverändert übermittelt werden. Darüber hinaus dürfen 15min Zeitabschnitte entfernt, geändert oder hinzugefügt werden. - Der zeitigste 15min Zeitabschnitt der übermittelten Lastanfrage, der gegenüber der bestehenden Lastanfrage eine Änderung darstellt, muss nach der aktuellen Viertelstunde liegen. - Der späteste geänderte oder hinzugefügte 15min Zeitabschnitt der Lastanfrage darf nicht weiter als zwei Tage in der Zukunft liegen.
var force = true; // bool | Steuert die API dahingehend, ob die übermittelte Lastanfrage ohne eine Kapazitätsprüfung aktzeptiert werden soll oder nicht. Siehe Parameter mit selben Namen unter `POST /schedule`.

try { 
    var result = api_instance.updateSchedule(scheduleID, scheduleRequest, force);
    print(result);
} catch (e) {
    print('Exception when calling BrokerApi->updateSchedule: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scheduleID** | **String**| ID einer bestätigen Lastanfrage. | 
 **scheduleRequest** | [**ScheduleRequest**](ScheduleRequest.md)| Die zu aktualisierende Lastanfrage.  Technische Forderungen: - Die Lastanfrage darf keine negativen Leistungswerte enthalten. - Die enthaltenen Zeitabschnitte müssen sich am Viertelstunden-Zeitraster orientieren.  Fachliche Forderungen: - Alle bestehende 15min Zeitabschnitte der Lastanfrage, die nunmehr in der Vergangenheit liegen, müssen unverändert übermittelt werden. Darüber hinaus dürfen 15min Zeitabschnitte entfernt, geändert oder hinzugefügt werden. - Der zeitigste 15min Zeitabschnitt der übermittelten Lastanfrage, der gegenüber der bestehenden Lastanfrage eine Änderung darstellt, muss nach der aktuellen Viertelstunde liegen. - Der späteste geänderte oder hinzugefügte 15min Zeitabschnitt der Lastanfrage darf nicht weiter als zwei Tage in der Zukunft liegen. | 
 **force** | **bool**| Steuert die API dahingehend, ob die übermittelte Lastanfrage ohne eine Kapazitätsprüfung aktzeptiert werden soll oder nicht. Siehe Parameter mit selben Namen unter `POST /schedule`. | [optional] [default to false]

### Return type

[**Schedule**](Schedule.md)

### Authorization

[flexclient-access](../README.md#flexclient-access)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

