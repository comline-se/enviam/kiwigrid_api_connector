//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:netzflex_incentives_api/src/model/incentive_item.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'incentive_series_all_of.g.dart';



abstract class IncentiveSeriesAllOf implements Built<IncentiveSeriesAllOf, IncentiveSeriesAllOfBuilder> {
    /// elements of the incentive series
    @BuiltValueField(wireName: r'items')
    BuiltList<IncentiveItem>? get items;

    IncentiveSeriesAllOf._();

    static void _initializeBuilder(IncentiveSeriesAllOfBuilder b) => b;

    factory IncentiveSeriesAllOf([void updates(IncentiveSeriesAllOfBuilder b)]) = _$IncentiveSeriesAllOf;

    @BuiltValueSerializer(custom: true)
    static Serializer<IncentiveSeriesAllOf> get serializer => _$IncentiveSeriesAllOfSerializer();
}

class _$IncentiveSeriesAllOfSerializer implements StructuredSerializer<IncentiveSeriesAllOf> {
    @override
    final Iterable<Type> types = const [IncentiveSeriesAllOf, _$IncentiveSeriesAllOf];

    @override
    final String wireName = r'IncentiveSeriesAllOf';

    @override
    Iterable<Object?> serialize(Serializers serializers, IncentiveSeriesAllOf object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.items != null) {
            result
                ..add(r'items')
                ..add(serializers.serialize(object.items,
                    specifiedType: const FullType(BuiltList, [FullType(IncentiveItem)])));
        }
        return result;
    }

    @override
    IncentiveSeriesAllOf deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = IncentiveSeriesAllOfBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'items':
                    result.items.replace(serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(IncentiveItem)])) as BuiltList<IncentiveItem>);
                    break;
            }
        }
        return result.build();
    }
}

