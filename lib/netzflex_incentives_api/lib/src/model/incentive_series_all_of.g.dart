// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'incentive_series_all_of.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$IncentiveSeriesAllOf extends IncentiveSeriesAllOf {
  @override
  final BuiltList<IncentiveItem>? items;

  factory _$IncentiveSeriesAllOf(
          [void Function(IncentiveSeriesAllOfBuilder)? updates]) =>
      (new IncentiveSeriesAllOfBuilder()..update(updates)).build();

  _$IncentiveSeriesAllOf._({this.items}) : super._();

  @override
  IncentiveSeriesAllOf rebuild(
          void Function(IncentiveSeriesAllOfBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  IncentiveSeriesAllOfBuilder toBuilder() =>
      new IncentiveSeriesAllOfBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is IncentiveSeriesAllOf && items == other.items;
  }

  @override
  int get hashCode {
    return $jf($jc(0, items.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('IncentiveSeriesAllOf')
          ..add('items', items))
        .toString();
  }
}

class IncentiveSeriesAllOfBuilder
    implements Builder<IncentiveSeriesAllOf, IncentiveSeriesAllOfBuilder> {
  _$IncentiveSeriesAllOf? _$v;

  ListBuilder<IncentiveItem>? _items;
  ListBuilder<IncentiveItem> get items =>
      _$this._items ??= new ListBuilder<IncentiveItem>();
  set items(ListBuilder<IncentiveItem>? items) => _$this._items = items;

  IncentiveSeriesAllOfBuilder() {
    IncentiveSeriesAllOf._initializeBuilder(this);
  }

  IncentiveSeriesAllOfBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _items = $v.items?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(IncentiveSeriesAllOf other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$IncentiveSeriesAllOf;
  }

  @override
  void update(void Function(IncentiveSeriesAllOfBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$IncentiveSeriesAllOf build() {
    _$IncentiveSeriesAllOf _$result;
    try {
      _$result = _$v ?? new _$IncentiveSeriesAllOf._(items: _items?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'items';
        _items?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'IncentiveSeriesAllOf', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
