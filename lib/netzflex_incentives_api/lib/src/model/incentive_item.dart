//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'incentive_item.g.dart';



abstract class IncentiveItem implements Built<IncentiveItem, IncentiveItemBuilder> {
    /// Start time of the incentive validity.
    @BuiltValueField(wireName: r'startTime')
    DateTime get startTime;

    /// Value of the incentive. 
    @BuiltValueField(wireName: r'value')
    num get value;

    /// Score of the incentive value. This is a value between -1 and +1 where +1 is the highest and -1 is the lowest score. 
    @BuiltValueField(wireName: r'score')
    num? get score;

    IncentiveItem._();

    static void _initializeBuilder(IncentiveItemBuilder b) => b;

    factory IncentiveItem([void updates(IncentiveItemBuilder b)]) = _$IncentiveItem;

    @BuiltValueSerializer(custom: true)
    static Serializer<IncentiveItem> get serializer => _$IncentiveItemSerializer();
}

class _$IncentiveItemSerializer implements StructuredSerializer<IncentiveItem> {
    @override
    final Iterable<Type> types = const [IncentiveItem, _$IncentiveItem];

    @override
    final String wireName = r'IncentiveItem';

    @override
    Iterable<Object?> serialize(Serializers serializers, IncentiveItem object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        result
            ..add(r'startTime')
            ..add(serializers.serialize(object.startTime,
                specifiedType: const FullType(DateTime)));
        result
            ..add(r'value')
            ..add(serializers.serialize(object.value,
                specifiedType: const FullType(num)));
        if (object.score != null) {
            result
                ..add(r'score')
                ..add(serializers.serialize(object.score,
                    specifiedType: const FullType(num)));
        }
        return result;
    }

    @override
    IncentiveItem deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = IncentiveItemBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'startTime':
                    result.startTime = serializers.deserialize(value,
                        specifiedType: const FullType(DateTime)) as DateTime;
                    break;
                case r'value':
                    result.value = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
                case r'score':
                    result.score = serializers.deserialize(value,
                        specifiedType: const FullType(num)) as num;
                    break;
            }
        }
        return result.build();
    }
}

