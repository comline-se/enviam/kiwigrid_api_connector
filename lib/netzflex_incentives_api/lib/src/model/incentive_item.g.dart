// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'incentive_item.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$IncentiveItem extends IncentiveItem {
  @override
  final DateTime startTime;
  @override
  final num value;
  @override
  final num? score;

  factory _$IncentiveItem([void Function(IncentiveItemBuilder)? updates]) =>
      (new IncentiveItemBuilder()..update(updates)).build();

  _$IncentiveItem._({required this.startTime, required this.value, this.score})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        startTime, 'IncentiveItem', 'startTime');
    BuiltValueNullFieldError.checkNotNull(value, 'IncentiveItem', 'value');
  }

  @override
  IncentiveItem rebuild(void Function(IncentiveItemBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  IncentiveItemBuilder toBuilder() => new IncentiveItemBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is IncentiveItem &&
        startTime == other.startTime &&
        value == other.value &&
        score == other.score;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, startTime.hashCode), value.hashCode), score.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('IncentiveItem')
          ..add('startTime', startTime)
          ..add('value', value)
          ..add('score', score))
        .toString();
  }
}

class IncentiveItemBuilder
    implements Builder<IncentiveItem, IncentiveItemBuilder> {
  _$IncentiveItem? _$v;

  DateTime? _startTime;
  DateTime? get startTime => _$this._startTime;
  set startTime(DateTime? startTime) => _$this._startTime = startTime;

  num? _value;
  num? get value => _$this._value;
  set value(num? value) => _$this._value = value;

  num? _score;
  num? get score => _$this._score;
  set score(num? score) => _$this._score = score;

  IncentiveItemBuilder() {
    IncentiveItem._initializeBuilder(this);
  }

  IncentiveItemBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _startTime = $v.startTime;
      _value = $v.value;
      _score = $v.score;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(IncentiveItem other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$IncentiveItem;
  }

  @override
  void update(void Function(IncentiveItemBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$IncentiveItem build() {
    final _$result = _$v ??
        new _$IncentiveItem._(
            startTime: BuiltValueNullFieldError.checkNotNull(
                startTime, 'IncentiveItem', 'startTime'),
            value: BuiltValueNullFieldError.checkNotNull(
                value, 'IncentiveItem', 'value'),
            score: score);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
