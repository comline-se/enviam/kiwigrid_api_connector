//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:netzflex_incentives_api/src/model/incentive_meta_data.dart';
import 'package:built_collection/built_collection.dart';
import 'package:netzflex_incentives_api/src/model/incentive_series_all_of.dart';
import 'package:netzflex_incentives_api/src/model/incentive_item.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'incentive_series.g.dart';



abstract class IncentiveSeries implements Built<IncentiveSeries, IncentiveSeriesBuilder> {
    /// Unique id of the incentive
    @BuiltValueField(wireName: r'id')
    String? get id;

    /// name of the incentive
    @BuiltValueField(wireName: r'name')
    String? get name;

    /// Type of the incentive. Valid vaues are: * Bonus: The incentive a bonus. This means, the higher, the better. * Price: The incentive is a price. This means, the lower, the better.  * Percentage: The incentive is provided as a percentage value 0...100%.   * Other: The incentive is just a numeric value without predefined semantics
    @BuiltValueField(wireName: r'type')
    IncentiveSeriesTypeEnum? get type;
    // enum typeEnum {  Bonus,  Price,  Percentage,  Other,  };

    /// Duration of the used interval in seconds. This value is optional. If not provided then the default value of 900s is used.
    @BuiltValueField(wireName: r'duration')
    int? get duration;

    /// Unit of the incentive value.
    @BuiltValueField(wireName: r'unit')
    String? get unit;

    /// elements of the incentive series
    @BuiltValueField(wireName: r'items')
    BuiltList<IncentiveItem>? get items;

    IncentiveSeries._();

    static void _initializeBuilder(IncentiveSeriesBuilder b) => b;

    factory IncentiveSeries([void updates(IncentiveSeriesBuilder b)]) = _$IncentiveSeries;

    @BuiltValueSerializer(custom: true)
    static Serializer<IncentiveSeries> get serializer => _$IncentiveSeriesSerializer();
}

class _$IncentiveSeriesSerializer implements StructuredSerializer<IncentiveSeries> {
    @override
    final Iterable<Type> types = const [IncentiveSeries, _$IncentiveSeries];

    @override
    final String wireName = r'IncentiveSeries';

    @override
    Iterable<Object?> serialize(Serializers serializers, IncentiveSeries object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(String)));
        }
        if (object.name != null) {
            result
                ..add(r'name')
                ..add(serializers.serialize(object.name,
                    specifiedType: const FullType(String)));
        }
        if (object.type != null) {
            result
                ..add(r'type')
                ..add(serializers.serialize(object.type,
                    specifiedType: const FullType(IncentiveSeriesTypeEnum)));
        }
        if (object.duration != null) {
            result
                ..add(r'duration')
                ..add(serializers.serialize(object.duration,
                    specifiedType: const FullType(int)));
        }
        if (object.unit != null) {
            result
                ..add(r'unit')
                ..add(serializers.serialize(object.unit,
                    specifiedType: const FullType(String)));
        }
        if (object.items != null) {
            result
                ..add(r'items')
                ..add(serializers.serialize(object.items,
                    specifiedType: const FullType(BuiltList, [FullType(IncentiveItem)])));
        }
        return result;
    }

    @override
    IncentiveSeries deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = IncentiveSeriesBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'id':
                    result.id = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'name':
                    result.name = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'type':
                    result.type = serializers.deserialize(value,
                        specifiedType: const FullType(IncentiveSeriesTypeEnum)) as IncentiveSeriesTypeEnum;
                    break;
                case r'duration':
                    result.duration = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    break;
                case r'unit':
                    result.unit = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'items':
                    result.items.replace(serializers.deserialize(value,
                        specifiedType: const FullType(BuiltList, [FullType(IncentiveItem)])) as BuiltList<IncentiveItem>);
                    break;
            }
        }
        return result.build();
    }
}

class IncentiveSeriesTypeEnum extends EnumClass {

  /// Type of the incentive. Valid vaues are: * Bonus: The incentive a bonus. This means, the higher, the better. * Price: The incentive is a price. This means, the lower, the better.  * Percentage: The incentive is provided as a percentage value 0...100%.   * Other: The incentive is just a numeric value without predefined semantics
  @BuiltValueEnumConst(wireName: r'Bonus')
  static const IncentiveSeriesTypeEnum bonus = _$incentiveSeriesTypeEnum_bonus;
  /// Type of the incentive. Valid vaues are: * Bonus: The incentive a bonus. This means, the higher, the better. * Price: The incentive is a price. This means, the lower, the better.  * Percentage: The incentive is provided as a percentage value 0...100%.   * Other: The incentive is just a numeric value without predefined semantics
  @BuiltValueEnumConst(wireName: r'Price')
  static const IncentiveSeriesTypeEnum price = _$incentiveSeriesTypeEnum_price;
  /// Type of the incentive. Valid vaues are: * Bonus: The incentive a bonus. This means, the higher, the better. * Price: The incentive is a price. This means, the lower, the better.  * Percentage: The incentive is provided as a percentage value 0...100%.   * Other: The incentive is just a numeric value without predefined semantics
  @BuiltValueEnumConst(wireName: r'Percentage')
  static const IncentiveSeriesTypeEnum percentage = _$incentiveSeriesTypeEnum_percentage;
  /// Type of the incentive. Valid vaues are: * Bonus: The incentive a bonus. This means, the higher, the better. * Price: The incentive is a price. This means, the lower, the better.  * Percentage: The incentive is provided as a percentage value 0...100%.   * Other: The incentive is just a numeric value without predefined semantics
  @BuiltValueEnumConst(wireName: r'Other')
  static const IncentiveSeriesTypeEnum other = _$incentiveSeriesTypeEnum_other;

  static Serializer<IncentiveSeriesTypeEnum> get serializer => _$incentiveSeriesTypeEnumSerializer;

  const IncentiveSeriesTypeEnum._(String name): super(name);

  static BuiltSet<IncentiveSeriesTypeEnum> get values => _$incentiveSeriesTypeEnumValues;
  static IncentiveSeriesTypeEnum valueOf(String name) => _$incentiveSeriesTypeEnumValueOf(name);
}

