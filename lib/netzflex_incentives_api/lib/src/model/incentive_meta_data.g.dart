// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'incentive_meta_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const IncentiveMetaDataTypeEnum _$incentiveMetaDataTypeEnum_bonus =
    const IncentiveMetaDataTypeEnum._('bonus');
const IncentiveMetaDataTypeEnum _$incentiveMetaDataTypeEnum_price =
    const IncentiveMetaDataTypeEnum._('price');
const IncentiveMetaDataTypeEnum _$incentiveMetaDataTypeEnum_percentage =
    const IncentiveMetaDataTypeEnum._('percentage');
const IncentiveMetaDataTypeEnum _$incentiveMetaDataTypeEnum_other =
    const IncentiveMetaDataTypeEnum._('other');

IncentiveMetaDataTypeEnum _$incentiveMetaDataTypeEnumValueOf(String name) {
  switch (name) {
    case 'bonus':
      return _$incentiveMetaDataTypeEnum_bonus;
    case 'price':
      return _$incentiveMetaDataTypeEnum_price;
    case 'percentage':
      return _$incentiveMetaDataTypeEnum_percentage;
    case 'other':
      return _$incentiveMetaDataTypeEnum_other;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<IncentiveMetaDataTypeEnum> _$incentiveMetaDataTypeEnumValues =
    new BuiltSet<IncentiveMetaDataTypeEnum>(const <IncentiveMetaDataTypeEnum>[
  _$incentiveMetaDataTypeEnum_bonus,
  _$incentiveMetaDataTypeEnum_price,
  _$incentiveMetaDataTypeEnum_percentage,
  _$incentiveMetaDataTypeEnum_other,
]);

Serializer<IncentiveMetaDataTypeEnum> _$incentiveMetaDataTypeEnumSerializer =
    new _$IncentiveMetaDataTypeEnumSerializer();

class _$IncentiveMetaDataTypeEnumSerializer
    implements PrimitiveSerializer<IncentiveMetaDataTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'bonus': 'Bonus',
    'price': 'Price',
    'percentage': 'Percentage',
    'other': 'Other',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'Bonus': 'bonus',
    'Price': 'price',
    'Percentage': 'percentage',
    'Other': 'other',
  };

  @override
  final Iterable<Type> types = const <Type>[IncentiveMetaDataTypeEnum];
  @override
  final String wireName = 'IncentiveMetaDataTypeEnum';

  @override
  Object serialize(Serializers serializers, IncentiveMetaDataTypeEnum object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  IncentiveMetaDataTypeEnum deserialize(
          Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      IncentiveMetaDataTypeEnum.valueOf(
          _fromWire[serialized] ?? serialized as String);
}

class _$IncentiveMetaData extends IncentiveMetaData {
  @override
  final String? id;
  @override
  final String? name;
  @override
  final IncentiveMetaDataTypeEnum? type;
  @override
  final int? duration;
  @override
  final String? unit;

  factory _$IncentiveMetaData(
          [void Function(IncentiveMetaDataBuilder)? updates]) =>
      (new IncentiveMetaDataBuilder()..update(updates)).build();

  _$IncentiveMetaData._(
      {this.id, this.name, this.type, this.duration, this.unit})
      : super._();

  @override
  IncentiveMetaData rebuild(void Function(IncentiveMetaDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  IncentiveMetaDataBuilder toBuilder() =>
      new IncentiveMetaDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is IncentiveMetaData &&
        id == other.id &&
        name == other.name &&
        type == other.type &&
        duration == other.duration &&
        unit == other.unit;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, id.hashCode), name.hashCode), type.hashCode),
            duration.hashCode),
        unit.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('IncentiveMetaData')
          ..add('id', id)
          ..add('name', name)
          ..add('type', type)
          ..add('duration', duration)
          ..add('unit', unit))
        .toString();
  }
}

class IncentiveMetaDataBuilder
    implements Builder<IncentiveMetaData, IncentiveMetaDataBuilder> {
  _$IncentiveMetaData? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  IncentiveMetaDataTypeEnum? _type;
  IncentiveMetaDataTypeEnum? get type => _$this._type;
  set type(IncentiveMetaDataTypeEnum? type) => _$this._type = type;

  int? _duration;
  int? get duration => _$this._duration;
  set duration(int? duration) => _$this._duration = duration;

  String? _unit;
  String? get unit => _$this._unit;
  set unit(String? unit) => _$this._unit = unit;

  IncentiveMetaDataBuilder() {
    IncentiveMetaData._initializeBuilder(this);
  }

  IncentiveMetaDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _type = $v.type;
      _duration = $v.duration;
      _unit = $v.unit;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(IncentiveMetaData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$IncentiveMetaData;
  }

  @override
  void update(void Function(IncentiveMetaDataBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$IncentiveMetaData build() {
    final _$result = _$v ??
        new _$IncentiveMetaData._(
            id: id, name: name, type: type, duration: duration, unit: unit);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
