// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'incentive_series.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const IncentiveSeriesTypeEnum _$incentiveSeriesTypeEnum_bonus =
    const IncentiveSeriesTypeEnum._('bonus');
const IncentiveSeriesTypeEnum _$incentiveSeriesTypeEnum_price =
    const IncentiveSeriesTypeEnum._('price');
const IncentiveSeriesTypeEnum _$incentiveSeriesTypeEnum_percentage =
    const IncentiveSeriesTypeEnum._('percentage');
const IncentiveSeriesTypeEnum _$incentiveSeriesTypeEnum_other =
    const IncentiveSeriesTypeEnum._('other');

IncentiveSeriesTypeEnum _$incentiveSeriesTypeEnumValueOf(String name) {
  switch (name) {
    case 'bonus':
      return _$incentiveSeriesTypeEnum_bonus;
    case 'price':
      return _$incentiveSeriesTypeEnum_price;
    case 'percentage':
      return _$incentiveSeriesTypeEnum_percentage;
    case 'other':
      return _$incentiveSeriesTypeEnum_other;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<IncentiveSeriesTypeEnum> _$incentiveSeriesTypeEnumValues =
    new BuiltSet<IncentiveSeriesTypeEnum>(const <IncentiveSeriesTypeEnum>[
  _$incentiveSeriesTypeEnum_bonus,
  _$incentiveSeriesTypeEnum_price,
  _$incentiveSeriesTypeEnum_percentage,
  _$incentiveSeriesTypeEnum_other,
]);

Serializer<IncentiveSeriesTypeEnum> _$incentiveSeriesTypeEnumSerializer =
    new _$IncentiveSeriesTypeEnumSerializer();

class _$IncentiveSeriesTypeEnumSerializer
    implements PrimitiveSerializer<IncentiveSeriesTypeEnum> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'bonus': 'Bonus',
    'price': 'Price',
    'percentage': 'Percentage',
    'other': 'Other',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'Bonus': 'bonus',
    'Price': 'price',
    'Percentage': 'percentage',
    'Other': 'other',
  };

  @override
  final Iterable<Type> types = const <Type>[IncentiveSeriesTypeEnum];
  @override
  final String wireName = 'IncentiveSeriesTypeEnum';

  @override
  Object serialize(Serializers serializers, IncentiveSeriesTypeEnum object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  IncentiveSeriesTypeEnum deserialize(
          Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      IncentiveSeriesTypeEnum.valueOf(
          _fromWire[serialized] ?? serialized as String);
}

class _$IncentiveSeries extends IncentiveSeries {
  @override
  final String? id;
  @override
  final String? name;
  @override
  final IncentiveSeriesTypeEnum? type;
  @override
  final int? duration;
  @override
  final String? unit;
  @override
  final BuiltList<IncentiveItem>? items;

  factory _$IncentiveSeries([void Function(IncentiveSeriesBuilder)? updates]) =>
      (new IncentiveSeriesBuilder()..update(updates)).build();

  _$IncentiveSeries._(
      {this.id, this.name, this.type, this.duration, this.unit, this.items})
      : super._();

  @override
  IncentiveSeries rebuild(void Function(IncentiveSeriesBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  IncentiveSeriesBuilder toBuilder() =>
      new IncentiveSeriesBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is IncentiveSeries &&
        id == other.id &&
        name == other.name &&
        type == other.type &&
        duration == other.duration &&
        unit == other.unit &&
        items == other.items;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc($jc(0, id.hashCode), name.hashCode), type.hashCode),
                duration.hashCode),
            unit.hashCode),
        items.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('IncentiveSeries')
          ..add('id', id)
          ..add('name', name)
          ..add('type', type)
          ..add('duration', duration)
          ..add('unit', unit)
          ..add('items', items))
        .toString();
  }
}

class IncentiveSeriesBuilder
    implements Builder<IncentiveSeries, IncentiveSeriesBuilder> {
  _$IncentiveSeries? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  IncentiveSeriesTypeEnum? _type;
  IncentiveSeriesTypeEnum? get type => _$this._type;
  set type(IncentiveSeriesTypeEnum? type) => _$this._type = type;

  int? _duration;
  int? get duration => _$this._duration;
  set duration(int? duration) => _$this._duration = duration;

  String? _unit;
  String? get unit => _$this._unit;
  set unit(String? unit) => _$this._unit = unit;

  ListBuilder<IncentiveItem>? _items;
  ListBuilder<IncentiveItem> get items =>
      _$this._items ??= new ListBuilder<IncentiveItem>();
  set items(ListBuilder<IncentiveItem>? items) => _$this._items = items;

  IncentiveSeriesBuilder() {
    IncentiveSeries._initializeBuilder(this);
  }

  IncentiveSeriesBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _type = $v.type;
      _duration = $v.duration;
      _unit = $v.unit;
      _items = $v.items?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(IncentiveSeries other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$IncentiveSeries;
  }

  @override
  void update(void Function(IncentiveSeriesBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$IncentiveSeries build() {
    _$IncentiveSeries _$result;
    try {
      _$result = _$v ??
          new _$IncentiveSeries._(
              id: id,
              name: name,
              type: type,
              duration: duration,
              unit: unit,
              items: _items?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'items';
        _items?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'IncentiveSeries', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
