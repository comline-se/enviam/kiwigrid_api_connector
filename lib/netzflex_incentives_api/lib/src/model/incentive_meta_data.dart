//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'incentive_meta_data.g.dart';



abstract class IncentiveMetaData implements Built<IncentiveMetaData, IncentiveMetaDataBuilder> {
    /// Unique id of the incentive
    @BuiltValueField(wireName: r'id')
    String? get id;

    /// name of the incentive
    @BuiltValueField(wireName: r'name')
    String? get name;

    /// Type of the incentive. Valid vaues are: * Bonus: The incentive a bonus. This means, the higher, the better. * Price: The incentive is a price. This means, the lower, the better.  * Percentage: The incentive is provided as a percentage value 0...100%.   * Other: The incentive is just a numeric value without predefined semantics
    @BuiltValueField(wireName: r'type')
    IncentiveMetaDataTypeEnum? get type;
    // enum typeEnum {  Bonus,  Price,  Percentage,  Other,  };

    /// Duration of the used interval in seconds. This value is optional. If not provided then the default value of 900s is used.
    @BuiltValueField(wireName: r'duration')
    int? get duration;

    /// Unit of the incentive value.
    @BuiltValueField(wireName: r'unit')
    String? get unit;

    IncentiveMetaData._();

    static void _initializeBuilder(IncentiveMetaDataBuilder b) => b;

    factory IncentiveMetaData([void updates(IncentiveMetaDataBuilder b)]) = _$IncentiveMetaData;

    @BuiltValueSerializer(custom: true)
    static Serializer<IncentiveMetaData> get serializer => _$IncentiveMetaDataSerializer();
}

class _$IncentiveMetaDataSerializer implements StructuredSerializer<IncentiveMetaData> {
    @override
    final Iterable<Type> types = const [IncentiveMetaData, _$IncentiveMetaData];

    @override
    final String wireName = r'IncentiveMetaData';

    @override
    Iterable<Object?> serialize(Serializers serializers, IncentiveMetaData object,
        {FullType specifiedType = FullType.unspecified}) {
        final result = <Object?>[];
        if (object.id != null) {
            result
                ..add(r'id')
                ..add(serializers.serialize(object.id,
                    specifiedType: const FullType(String)));
        }
        if (object.name != null) {
            result
                ..add(r'name')
                ..add(serializers.serialize(object.name,
                    specifiedType: const FullType(String)));
        }
        if (object.type != null) {
            result
                ..add(r'type')
                ..add(serializers.serialize(object.type,
                    specifiedType: const FullType(IncentiveMetaDataTypeEnum)));
        }
        if (object.duration != null) {
            result
                ..add(r'duration')
                ..add(serializers.serialize(object.duration,
                    specifiedType: const FullType(int)));
        }
        if (object.unit != null) {
            result
                ..add(r'unit')
                ..add(serializers.serialize(object.unit,
                    specifiedType: const FullType(String)));
        }
        return result;
    }

    @override
    IncentiveMetaData deserialize(Serializers serializers, Iterable<Object?> serialized,
        {FullType specifiedType = FullType.unspecified}) {
        final result = IncentiveMetaDataBuilder();

        final iterator = serialized.iterator;
        while (iterator.moveNext()) {
            final key = iterator.current as String;
            iterator.moveNext();
            final Object? value = iterator.current;
            switch (key) {
                case r'id':
                    result.id = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'name':
                    result.name = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
                case r'type':
                    result.type = serializers.deserialize(value,
                        specifiedType: const FullType(IncentiveMetaDataTypeEnum)) as IncentiveMetaDataTypeEnum;
                    break;
                case r'duration':
                    result.duration = serializers.deserialize(value,
                        specifiedType: const FullType(int)) as int;
                    break;
                case r'unit':
                    result.unit = serializers.deserialize(value,
                        specifiedType: const FullType(String)) as String;
                    break;
            }
        }
        return result.build();
    }
}

class IncentiveMetaDataTypeEnum extends EnumClass {

  /// Type of the incentive. Valid vaues are: * Bonus: The incentive a bonus. This means, the higher, the better. * Price: The incentive is a price. This means, the lower, the better.  * Percentage: The incentive is provided as a percentage value 0...100%.   * Other: The incentive is just a numeric value without predefined semantics
  @BuiltValueEnumConst(wireName: r'Bonus')
  static const IncentiveMetaDataTypeEnum bonus = _$incentiveMetaDataTypeEnum_bonus;
  /// Type of the incentive. Valid vaues are: * Bonus: The incentive a bonus. This means, the higher, the better. * Price: The incentive is a price. This means, the lower, the better.  * Percentage: The incentive is provided as a percentage value 0...100%.   * Other: The incentive is just a numeric value without predefined semantics
  @BuiltValueEnumConst(wireName: r'Price')
  static const IncentiveMetaDataTypeEnum price = _$incentiveMetaDataTypeEnum_price;
  /// Type of the incentive. Valid vaues are: * Bonus: The incentive a bonus. This means, the higher, the better. * Price: The incentive is a price. This means, the lower, the better.  * Percentage: The incentive is provided as a percentage value 0...100%.   * Other: The incentive is just a numeric value without predefined semantics
  @BuiltValueEnumConst(wireName: r'Percentage')
  static const IncentiveMetaDataTypeEnum percentage = _$incentiveMetaDataTypeEnum_percentage;
  /// Type of the incentive. Valid vaues are: * Bonus: The incentive a bonus. This means, the higher, the better. * Price: The incentive is a price. This means, the lower, the better.  * Percentage: The incentive is provided as a percentage value 0...100%.   * Other: The incentive is just a numeric value without predefined semantics
  @BuiltValueEnumConst(wireName: r'Other')
  static const IncentiveMetaDataTypeEnum other = _$incentiveMetaDataTypeEnum_other;

  static Serializer<IncentiveMetaDataTypeEnum> get serializer => _$incentiveMetaDataTypeEnumSerializer;

  const IncentiveMetaDataTypeEnum._(String name): super(name);

  static BuiltSet<IncentiveMetaDataTypeEnum> get values => _$incentiveMetaDataTypeEnumValues;
  static IncentiveMetaDataTypeEnum valueOf(String name) => _$incentiveMetaDataTypeEnumValueOf(name);
}

