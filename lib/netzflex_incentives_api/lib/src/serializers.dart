//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_import

import 'package:built_collection/built_collection.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:built_value/iso_8601_date_time_serializer.dart';
import 'package:netzflex_incentives_api/src/date_serializer.dart';
import 'package:netzflex_incentives_api/src/model/date.dart';

import 'package:netzflex_incentives_api/src/model/error_response.dart';
import 'package:netzflex_incentives_api/src/model/incentive_item.dart';
import 'package:netzflex_incentives_api/src/model/incentive_meta_data.dart';
import 'package:netzflex_incentives_api/src/model/incentive_series.dart';
import 'package:netzflex_incentives_api/src/model/incentive_series_all_of.dart';

part 'serializers.g.dart';

@SerializersFor([
  ErrorResponse,
  IncentiveItem,
  IncentiveMetaData,
  IncentiveSeries,
  IncentiveSeriesAllOf,
])
Serializers serializers = (_$serializers.toBuilder()
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(IncentiveMetaData)]),
        () => ListBuilder<IncentiveMetaData>(),
      )
      ..add(const DateSerializer())
      ..add(Iso8601DateTimeSerializer()))
    .build();

Serializers standardSerializers =
    (serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
