# netzflex_incentives_api.model.IncentiveSeries

## Load the model package
```dart
import 'package:netzflex_incentives_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Unique id of the incentive | [optional] 
**name** | **String** | name of the incentive | [optional] 
**type** | **String** | Type of the incentive. Valid vaues are: * Bonus: The incentive a bonus. This means, the higher, the better. * Price: The incentive is a price. This means, the lower, the better.  * Percentage: The incentive is provided as a percentage value 0...100%.   * Other: The incentive is just a numeric value without predefined semantics | [optional] 
**duration** | **int** | Duration of the used interval in seconds. This value is optional. If not provided then the default value of 900s is used. | [optional] 
**unit** | **String** | Unit of the incentive value. | [optional] 
**items** | [**BuiltList<IncentiveItem>**](IncentiveItem.md) | elements of the incentive series | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


