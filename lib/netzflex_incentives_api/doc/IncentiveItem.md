# netzflex_incentives_api.model.IncentiveItem

## Load the model package
```dart
import 'package:netzflex_incentives_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startTime** | [**DateTime**](DateTime.md) | Start time of the incentive validity. | 
**value** | **num** | Value of the incentive.  | 
**score** | **num** | Score of the incentive value. This is a value between -1 and +1 where +1 is the highest and -1 is the lowest score.  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


