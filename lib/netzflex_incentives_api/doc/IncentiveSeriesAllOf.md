# netzflex_incentives_api.model.IncentiveSeriesAllOf

## Load the model package
```dart
import 'package:netzflex_incentives_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**BuiltList<IncentiveItem>**](IncentiveItem.md) | elements of the incentive series | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


