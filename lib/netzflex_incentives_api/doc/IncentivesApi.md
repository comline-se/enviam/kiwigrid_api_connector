# netzflex_incentives_api.api.IncentivesApi

## Load the API package
```dart
import 'package:netzflex_incentives_api/api.dart';
```

All URIs are relative to *http://localhost:8080/incentives/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAllIncentives**](IncentivesApi.md#getallincentives) | **get** /incentive | 
[**getIncentives**](IncentivesApi.md#getincentives) | **get** /incentive/{incentiveID}/{fromTS}/{toTS} | 
[**getIncentivesForToday**](IncentivesApi.md#getincentivesfortoday) | **get** /incentive/{incentiveID} | 


# **getAllIncentives**
> BuiltList<IncentiveMetaData> getAllIncentives()



Return meta data for all available incentives.

### Example 
```dart
import 'package:netzflex_incentives_api/api.dart';

var api_instance = new IncentivesApi();

try { 
    var result = api_instance.getAllIncentives();
    print(result);
} catch (e) {
    print('Exception when calling IncentivesApi->getAllIncentives: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**BuiltList<IncentiveMetaData>**](IncentiveMetaData.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getIncentives**
> IncentiveSeries getIncentives(incentiveID, fromTS, toTS)



Return the incentive identified by the ID for the provided time range.

### Example 
```dart
import 'package:netzflex_incentives_api/api.dart';

var api_instance = new IncentivesApi();
var incentiveID = incentiveID_example; // String | ID of an active Incentive
var fromTS = 789; // int | Start time stamp for the incentives
var toTS = 789; // int | End time stamp for the incentives

try { 
    var result = api_instance.getIncentives(incentiveID, fromTS, toTS);
    print(result);
} catch (e) {
    print('Exception when calling IncentivesApi->getIncentives: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **incentiveID** | **String**| ID of an active Incentive | 
 **fromTS** | **int**| Start time stamp for the incentives | 
 **toTS** | **int**| End time stamp for the incentives | 

### Return type

[**IncentiveSeries**](IncentiveSeries.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getIncentivesForToday**
> IncentiveSeries getIncentivesForToday(incentiveID)



Return the incentive identified by the ID for the current day

### Example 
```dart
import 'package:netzflex_incentives_api/api.dart';

var api_instance = new IncentivesApi();
var incentiveID = incentiveID_example; // String | ID of an active Incentive

try { 
    var result = api_instance.getIncentivesForToday(incentiveID);
    print(result);
} catch (e) {
    print('Exception when calling IncentivesApi->getIncentivesForToday: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **incentiveID** | **String**| ID of an active Incentive | 

### Return type

[**IncentiveSeries**](IncentiveSeries.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

