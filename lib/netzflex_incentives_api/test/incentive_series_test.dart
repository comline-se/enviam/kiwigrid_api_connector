import 'package:test/test.dart';
import 'package:netzflex_incentives_api/netzflex_incentives_api.dart';

// tests for IncentiveSeries
void main() {
  final instance = IncentiveSeriesBuilder();
  // TODO add properties to the builder and call build()

  group(IncentiveSeries, () {
    // Unique id of the incentive
    // String id
    test('to test the property `id`', () async {
      // TODO
    });

    // name of the incentive
    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // Type of the incentive. Valid vaues are: * Bonus: The incentive a bonus. This means, the higher, the better. * Price: The incentive is a price. This means, the lower, the better.  * Percentage: The incentive is provided as a percentage value 0...100%.   * Other: The incentive is just a numeric value without predefined semantics
    // String type
    test('to test the property `type`', () async {
      // TODO
    });

    // Duration of the used interval in seconds. This value is optional. If not provided then the default value of 900s is used.
    // int duration
    test('to test the property `duration`', () async {
      // TODO
    });

    // Unit of the incentive value.
    // String unit
    test('to test the property `unit`', () async {
      // TODO
    });

    // elements of the incentive series
    // BuiltList<IncentiveItem> items
    test('to test the property `items`', () async {
      // TODO
    });

  });
}
