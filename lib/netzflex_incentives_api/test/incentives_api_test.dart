import 'package:test/test.dart';
import 'package:netzflex_incentives_api/netzflex_incentives_api.dart';


/// tests for IncentivesApi
void main() {
  final instance = NetzflexIncentivesApi().getIncentivesApi();

  group(IncentivesApi, () {
    // Return meta data for all available incentives.
    //
    //Future<BuiltList<IncentiveMetaData>> getAllIncentives() async
    test('test getAllIncentives', () async {
      // TODO
    });

    // Return the incentive identified by the ID for the provided time range.
    //
    //Future<IncentiveSeries> getIncentives(String incentiveID, int fromTS, int toTS) async
    test('test getIncentives', () async {
      // TODO
    });

    // Return the incentive identified by the ID for the current day
    //
    //Future<IncentiveSeries> getIncentivesForToday(String incentiveID) async
    test('test getIncentivesForToday', () async {
      // TODO
    });

  });
}
