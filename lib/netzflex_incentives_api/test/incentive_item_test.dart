import 'package:test/test.dart';
import 'package:netzflex_incentives_api/netzflex_incentives_api.dart';

// tests for IncentiveItem
void main() {
  final instance = IncentiveItemBuilder();
  // TODO add properties to the builder and call build()

  group(IncentiveItem, () {
    // Start time of the incentive validity.
    // DateTime startTime
    test('to test the property `startTime`', () async {
      // TODO
    });

    // Value of the incentive. 
    // num value
    test('to test the property `value`', () async {
      // TODO
    });

    // Score of the incentive value. This is a value between -1 and +1 where +1 is the highest and -1 is the lowest score. 
    // num score
    test('to test the property `score`', () async {
      // TODO
    });

  });
}
