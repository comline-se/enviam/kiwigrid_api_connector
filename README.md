# kiwigrid_api_connector

This package contains the generated api libraries to connect to kiwigrid services.

## Getting Started

To generate the code from an openapi-spec, add new class entry for the config, and reference it to the openapi-spec file in the same directory 

After that, run `./generate.sh` to generate the api libaries.

**Attention:**

As the dart.dioNext config is still experimental but the only nullSafe generator variant, It may occur that generation is not complileable from the start, so it might need manual corrections. 

To include a package, reference it in your app pubspec.yaml:

```
  heat_control_api:
    git:
      url: https://gitlab.com/comline-se/enviam/kiwigrid_api_connector
      path: lib/heat_control_api
```

```
  netzflex_broker_api:
    git:
      url: https://gitlab.com/comline-se/enviam/kiwigrid_api_connector
      path: lib/netzflex_broker_api
```

```
  netxflex_incentives_api:
    git:
      url: https://gitlab.com/comline-se/enviam/kiwigrid_api_connector
      path: lib/netzflex_incentives_api
```